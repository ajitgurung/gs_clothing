<?php

use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(WelcomeController::class)->group(function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('welcome');

    Route::get('product/{slug}', 'product')->name('product');
    Route::get('shop', 'shop')->name('shop');
    Route::post('product/variation-info', 'variation')->name('product.variation');
});
Route::namespace('App\Http\Controllers')->group(function () {

    Route::get('admin/login', 'Auth\LoginController@loginForm')->name('login');
    Route::post('securelogin', 'Auth\LoginController@login')->name('securelogin');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::group(['middleware' => ['auth']], function () {
        Route::namespace('Admin')->name('admin.')->prefix('admin')->group(function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');

            // Setting Routes
            Route::get('/setting', 'SettingController@index')->name('setting');
            Route::post('/setting/update', 'SettingController@update')->name('setting.update');
            // Setting Routes end

            //  Users Route
            Route::name('users.')->prefix('users')->group(function () {
                Route::get('/', 'UserController@index')->name('index');
                Route::get('/customer', 'UserController@customer')->name('customer');
                Route::get('/create', 'UserController@create')->name('create');
                Route::post('/', 'UserController@store')->name('store');
                Route::get('/{id}/show', 'UserController@show')->name('show');
                Route::get('/{id}/edit', 'UserController@edit')->name('edit');
                Route::patch('/{id}', 'UserController@update')->name('update');
                Route::get('/delete/{id}', 'UserController@destroy')->name('destroy');
            });
            //  Users Route End

            Route::name('slider.')->prefix('slider')->group(function () {
                Route::get('/', 'SliderController@index')->name('index');
                Route::post('/set_order', 'SliderController@set_order')->name('order');
                Route::post('/store', 'SliderController@store')->name('store');
                Route::get('/edit/{id}', 'SliderController@edit')->name('edit');
                Route::post('/update/{id}', 'SliderController@update')->name('update');
                Route::get('/delete/{id}', 'SliderController@destroy')->name('destroy');
            });

            Route::name('category.')->prefix('category')->group(function () {
                Route::get('/', 'CategoryController@index')->name('index');
                Route::post('/set_order', 'CategoryController@set_order')->name('order');
                Route::post('/store', 'CategoryController@store')->name('store');
                Route::get('/edit/{id}', 'CategoryController@edit')->name('edit');
                Route::post('/update/{id}', 'CategoryController@update')->name('update');
                Route::get('/delete/{id}', 'CategoryController@destroy')->name('destroy');
            });

            Route::name('brand.')->prefix('brand')->group(function () {
                Route::get('/', 'BrandController@index')->name('index');
                Route::post('/set_order', 'BrandController@set_order')->name('order');
                Route::post('/store', 'BrandController@store')->name('store');
                Route::get('/edit/{id}', 'BrandController@edit')->name('edit');
                Route::post('/update/{id}', 'BrandController@update')->name('update');
                Route::get('/delete/{id}', 'BrandController@destroy')->name('destroy');
            });

            // Size Chart
            Route::name('sizechart.')->prefix('sizechart')->group(function () {
                Route::get('/', 'SizeChartController@index')->name('index');
                Route::post('/set_order', 'SizeChartController@set_order')->name('order');
                Route::post('/store', 'SizeChartController@store')->name('store');
                Route::get('/edit/{id}', 'SizeChartController@edit')->name('edit');
                Route::patch('/update/{id}', 'SizeChartController@update')->name('update');
                Route::get('/delete/{id}', 'SizeChartController@destroy')->name('destroy');
            });
            // Size chart end

            // Size
            Route::name('size.')->prefix('size')->group(function () {
                Route::get('/', 'SizeController@index')->name('index');
                Route::post('/set_order', 'SizeController@set_order')->name('order');
                Route::post('/store', 'SizeController@store')->name('store');
                Route::get('/edit/{id}', 'SizeController@edit')->name('edit');
                Route::patch('/update/{id}', 'SizeController@update')->name('update');
                Route::get('/delete/{id}', 'SizeController@destroy')->name('destroy');
            });
            // Size ends

            Route::name('product.')->prefix('product')->group(function () {
                Route::get('/', 'ProductController@index')->name('index');
                Route::get('/create', 'ProductController@create')->name('create');
                Route::post('/store', 'ProductController@store')->name('store');
                Route::get('/edit/{id}', 'ProductController@edit')->name('edit');
                Route::patch('/update/{id}', 'ProductController@update')->name('update');
                Route::get('/delete/{id}', 'ProductController@destroy')->name('destroy');
                Route::post('/update/status', 'ProductController@status')->name('status');
                Route::post('/update/featured', 'ProductController@featured')->name('featured');
                Route::get('/delete-image/{albumName}/{photoName}', 'ProductController@delete_gallery_image');

                Route::name('variation.')->group(function () {
                    Route::get('/color-and-sizes/{slug}', 'ProductController@variation')->name('index');
                    Route::get('/add-color-and-sizes/{slug}', 'ProductController@addVariationForm')->name('create');
                    Route::post('/add-color-and-sizes/{slug}', 'ProductController@addVariation')->name('store');
                    Route::get('/edit-color-and-sizes/{slug}/{id}', 'ProductController@editVariation')->name('edit');
                    Route::patch('/update-color-and-sizes/{slug}/{id}', 'ProductController@updateVariation')->name('update');
                    Route::get('/color-and-sizes/{slug}/delete/{id}', 'ProductController@deleteVariation')->name('delete');
                    Route::get('/delete-variation-image/{id}/{photoName}', 'ProductController@delete_variation_image');
                });
            });

            Route::name('faq.')->prefix('faq')->group(function () {
                Route::get('/', 'FaqController@index')->name('index');
                Route::post('/set_order', 'FaqController@set_order')->name('order');
                Route::post('/store', 'FaqController@store')->name('store');
                Route::get('/edit/{id}', 'FaqController@edit')->name('edit');
                Route::post('/update/{id}', 'FaqController@update')->name('update');
                Route::get('/delete/{id}', 'FaqController@destroy')->name('destroy');
            });

            Route::name('blog.')->prefix('blog')->group(function () {
                Route::get('/', 'BlogController@index')->name('index');
                Route::get('/create', 'BlogController@create')->name('create');
                Route::post('/store', 'BlogController@store')->name('store');
                Route::get('/edit/{id}', 'BlogController@edit')->name('edit');
                Route::post('/update/{id}', 'BlogController@update')->name('update');
                Route::get('/delete/{id}', 'BlogController@destroy')->name('destroy');
                Route::post('/update/status', 'BlogController@status')->name('status');
            });

            Route::get('/terms-and-condition', 'TermsController@index')->name('terms');
            Route::post('/terms-and-condition/update', 'TermsController@update')->name('terms.update');

            Route::get('/policy', 'PolicyController@index')->name('policy');
            Route::post('/policy/update', 'PolicyController@update')->name('policy.update');
        });
    });
});
