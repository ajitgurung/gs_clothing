<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Size;
use App\Models\Variation;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function product($slug)
    {
        $product = Product::where('slug', $slug)->where('display', 1)->firstOrFail();

        return view('product', compact('product'));
    }

    public function variation(Request $request)
    {
        $variation = Variation::findOrFail(base64_decode($request->id));
        $sizes = Size::orderBy('order_item')->get();

        $var_sizes = json_decode($variation->sizes);
        $slider = view('partial.variation-slider', compact('variation'))->render();
        $size = view('partial.variation-size', compact('sizes', 'var_sizes'))->render();

        return response()->json(['slider' => $slider, 'size' => $size]);
    }

    public function shop()
    {
        $products = Product::where('display', 1)->get();
        $categories = Category::where('status', 1)->get();
        $sizes = Size::get();
        $brands = Brand::get();

        return view('shop', compact('products', 'categories', 'sizes', 'brands'));
    }
}
