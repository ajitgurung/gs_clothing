<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Models\CropImg;

class SettingController extends Controller
{
    public function index()
    {
        $setting = Setting::findOrFail(1);
        return view('admin.setting.index', compact('setting'));
    }

    public function update(Request $request)
    {
        $setting = Setting::findOrFail('1');
        $validatedData = $request->validate([
            'sitetitle' => 'required|max:255',
            'siteemail' => 'required|max:225|email',
        ]);
        $setting->sitetitle = $request->sitetitle;
        $setting->siteemail = $request->siteemail;
        $setting->address = $request->address;
        $setting->phone = $request->phone;
        $setting->mobile = $request->mobile;
        $setting->phone2 = $request->phone2;
        $setting->mobile2 = $request->mobile2;
        $setting->hotline = $request->hotline;
        $setting->whatsapp = $request->whatsapp;
        $setting->viber = $request->viber;
        $setting->facebookurl = $request->facebookurl;
        $setting->instagramurl = $request->instagramurl;
        $setting->sitekeyword = $request->sitekeyword;
        $setting->googlemapurl = $request->googlemapurl;
        $setting->min_order_amt = $request->min_order_amt;
        $setting->max_order_qty = $request->max_order_qty;
        $setting->notification_email = $request->notification_email;

        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $filename = time() . '.' . $logo->getClientOriginalExtension();
            $oldlogo = $setting->logo;
            $validatedData = $request->validate([
                'logo' => 'image|mimes:jpeg,png,jpg|max:1000',
            ]);

            Storage::putFileAs('public/setting/logo', new File($logo), $filename);

            $setting->logo = $filename;

            CropImg::resize_crop_images(200, 200, $logo, "public/setting/logo/thumb_" . $filename);
            if ($oldlogo != null) {
                //deleting exiting logo
                Storage::delete('public/setting/logo/' . $oldlogo);
                Storage::delete('public/setting/logo/thumb_' . $oldlogo);
            }
        }
        if ($request->hasFile('header_image')) {
            $header_image = $request->file('header_image');
            $filename = time() . '.' . $header_image->getClientOriginalExtension();
            $oldheader_image = $setting->header_image;
            $validatedData = $request->validate([
                'header_image' => 'image|mimes:jpeg,png,jpg|max:1000',
            ]);

            Storage::putFileAs('public/setting/header_image', new File($header_image), $filename);

            $setting->header_image = $filename;

            CropImg::resize_crop_images(200, 200, $header_image, "public/setting/header_image/thumb_" . $filename);
            if ($oldheader_image != null) {
                //deleting exiting header_image
                Storage::delete('public/setting/header_image/' . $oldheader_image);
                Storage::delete('public/setting/header_image/thumb_' . $oldheader_image);
            }
        }

        if ($request->hasFile('favicon')) {
            $logo = $request->file('favicon');
            $filename = time() . '.' . $logo->getClientOriginalExtension();
            $oldfavicon = $setting->favicon;
            $validatedData = $request->validate([
                'favicon' => 'image|mimes:jpeg,png,jpg|max:1000',
            ]);

            Storage::putFileAs('public/setting/favicon', new File($logo), $filename);

            $setting->favicon = $filename;

            CropImg::resize_crop_images(200, 200, $logo, "public/setting/favicon/thumb_" . $filename);
            if ($oldfavicon != null) {
                //deleting exiting logo
                Storage::delete('public/setting/favicon/' . $oldfavicon);
                Storage::delete('public/setting/favicon/thumb_' . $oldfavicon);
            }
        }
        $setting->save();

        return redirect('admin/setting')->with('status', 'Setting Update Successfully.');
    }
}
