<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\CropImg;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function index(){
        $items = Blog::orderBy('created_at', 'DESC')->get();
        return view('admin.blog.index', compact('items'));
    }

    public function status(Request $request)
    {
        // return json_encode($request);
        $id = $request->id;
        $status = Blog::findOrFail($id);
        if ($status->status === 1) {
            $status->status = 0;
        } else {
            $status->status = 1;
        }
        $status->save();
        $response = array('success' => 'Status Updated Successfully !!');
        return json_encode($response);
    }

    public function create(){
        return view('admin.blog.form');
    }

    public function store(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'banner' => 'required',
            'short_content' => 'required'
        ]);

        $item = new Blog();
        $item->title = $request->title;

        $slug = Blog::createSlug($request->title, 'id', 0);
        $item->slug = $slug;

        $item->short_content = $request->short_content;
        $item->content = $request->content;

        if($request->status){
            $item->status = 1;
        }

        $path = public_path() . '/storage/blog/'. $slug;
        $folderPath = 'public/blog/'. $slug;
        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
        }

        if ($request->hasFile('banner')) {
            $validatedData = $request->validate([
                'banner' => 'image|mimes:jpeg,png,jpg|max:50000',
            ]);

            $file = $request->file('banner');
            $fileName = time() . '.' . $file->getClientOriginalExtension();

            CropImg::resize_crop_images(1200, 800, $file, $folderPath . "/" . $fileName);
            CropImg::resize_crop_images(120, 80, $file, $folderPath . "/small_" . $fileName);
            $item->banner = $fileName;
        }
        $item->save();
        return redirect(route('admin.blog.index'))->with('status', 'Blog created successfully');
    }

    public function edit($slug){
        $item = Blog::where('slug', $slug)->first();
        return view('admin.blog.form', compact('item'));
    }

    public function update(Request $request, $slug){
        $this->validate($request, [
            'title' => 'required'
        ]);

        $item = Blog::where('slug', $slug)->first();
        if($item->title != $request->title){
            $slug = Blog::createSlug($request->title, 'id', 0);
        }
        $item->title = $request->title;
        $item->short_content = $request->short_content;
        $item->content = $request->content;

        if($request->status){
            $item->status = 1;
        }else{
            $item->status = 0;
        }

        $path = public_path() . '/storage/blog/'. $item->slug;
        if(isset($slug)){
            if ($item->slug != $slug) {
                $oldslug = $item->slug;
                $item->slug = $slug;
                if (file_exists($path)) {
                    Storage::move('public/blog/' . $oldslug, 'public/blog/' . $slug);
                }
            }
        }else{
            $slug = $item->slug;
            $path = public_path() . '/storage/blog/' . $slug;
        }
        $folderPath = 'public/blog/' . $slug;
        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
        }

        if ($request->hasFile('banner')) {
            $validatedData = $request->validate([
                'banner' => 'image|mimes:jpeg,png,jpg|max:50000',
            ]);
            $oldImage = $item->banner;

            $file = $request->file('banner');
            $fileName = time() . '.' . $file->getClientOriginalExtension();

            CropImg::resize_crop_images(1200, 800, $file, $folderPath . "/" . $fileName);
            CropImg::resize_crop_images(120, 80, $file, $folderPath . "/small_" . $fileName);
            $item->banner = $fileName;
            if($oldImage !== null && $oldImage !== ''){
                Storage::delete('public/blog/'. $slug . '/' . $oldImage);
                Storage::delete('public/blog/'. $slug . '/small_' . $oldImage);
            }
        }
        $item->update();
        return redirect(route('admin.blog.index'))->with('status', 'Blog content updated successfully');
    }

    public function destroy($slug){
        $item = Blog::where('slug', $slug)->first();
        if($item->slug !== null && $item->slug !== ''){
            Storage::deleteDirectory('public/blog/' . $item->slug);
        }
        $item->delete();
        return redirect()->back()->with('status', 'Blog content deleted successfully!');
    }
}
