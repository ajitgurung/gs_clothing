<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\CropImg;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    public function index(){
        $sliders = Slider::orderBy('order_item')->get();

        // foreach($sliders as $slider){
        //     if($slider->image != null){
        //         $oldImg = $slider->image;
        //         $fileName = time(). '.webp';
                
        //         $cover = storage_path('app/public/slider/' . $fileName);
        //         $file = Storage::disk('public')->get('slider/'. $oldImg);
        //         Image::make($file)->encode('webp')->save($cover);

        //         $thumbcover = storage_path('app/public/slider/' . $fileName);
        //         $thumbfile = Storage::disk('public')->get('slider/thumbs/'. $oldImg);
        //         Image::make($thumbfile)->encode('webp')->save($thumbcover);

        //         $slider->image = $fileName;
        //         $slider->save();
        //         Storage::delete('public/slider/'. $oldImg);
        //         Storage::delete('public/slider/thumbs/'. $oldImg);  
        //     }
        // }
        return view('admin.slider.index')->with('sliders', $sliders);
    }

    public function set_order(Request $request)
    {
        $list_order = [];
        $slider = new Slider;
        $list_order = $request['list_order'];
        $i = 1;
        foreach ($list_order as $id) {
            $updateData = array("order_item" => $i);
            Slider::where('id', $id)->update($updateData);
            $i++;
        }
        $data = array('status' => 'success');
        echo json_encode($data);
    }

    public function store(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'image'=> 'required'
        ]);
        $slider = new Slider();
        $slider->title = $request->title;

        $max_order = DB::table('sliders')->max('order_item');
        $slider->order_item = $max_order + 1;
        $slider->link = $request->link;
        if($request->status){
            $slider->status = 1;
        }else{
            $slider->status = 0;
        }

        $path = public_path() . '/storage/slider/';
        $folderPath = 'public/slider/';
        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
            if (!file_exists($path. '/thumbs')) {
                Storage::makeDirectory($folderPath . '/thumbs', 0777, true, true);
            }
        }

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg|max:50000',
            ]);
            $file = $request->file('image');
            $fileName = time() . '.' . 'webp';
           
            CropImg::resize_crop_images(1350, 465, $file, $folderPath . "/" . $fileName);
            CropImg::resize_crop_images(135, 46, $file, $folderPath . "/thumbs/" . $fileName);
            $slider->image = $fileName;
        }

        $slider->save();
        return redirect(route('admin.slider.index'))->with('status', 'Slider created successfully !!');
    }

    public function edit($id){
        $slider = Slider::findOrFail($id);
        return view('admin.slider.edit')->with('slider', $slider);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'title' => 'required'
        ]);
        $slider = Slider::findOrFail($id);
        $slider->title = $request->title;
        $slider->link = $request->link;
        if($request->status){
            $slider->status = 1;
        }else{
            $slider->status = 0;
        }

        $path = public_path() . '/storage/slider/';
        $folderPath = 'public/slider/';
        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
            Storage::makeDirectory($folderPath . '/thumbs', 0777, true, true);

        }
        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg|max:50000',
            ]);
            $oldImage = $slider->image;
            $file = $request->file('image');
            $fileName = time() . '.webp';

            CropImg::resize_crop_images(1350, 465, $file, $folderPath . "/" . $fileName);
            CropImg::resize_crop_images(135, 46, $file, $folderPath . "/thumbs/" . $fileName);
            $slider->image = $fileName;
            if($oldImage != null){
                Storage::delete('public/slider/' . $oldImage);
                Storage::delete('public/slider/thumbs/' . $oldImage);
            }
        }
        $slider->update();
        return redirect(route('admin.slider.index'))->with('status', 'Slider updated successfully !!');
    }

    public function destroy($id){
        $slider = Slider::findOrFail($id);
        if($slider->image != null){
            Storage::delete('public/slider/'. $slider->image);
            Storage::delete('public/slider/thumbs/'. $slider->image);
        }
        $slider->delete();
        return redirect()->back()->with('status', 'Slider deleted successfully!!');
    }
}
