<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Term;

class TermsController extends Controller
{
    public function index()
    {
        $term = Term::findOrFail(1);
        return view('admin.terms.index', compact('term'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'content' => 'required'
        ]);

        $term = Term::findOrFail(1);
        $term->content = $request->content;
        $term->update();
        
        return redirect(route('admin.terms'))->with('status', 'Terms & Conditions content updated successfully');
    }
}