<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Policy;
use App\Models\CropImg;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class PolicyController extends Controller
{
    public function index()
    {
        $policy = Policy::findOrFail(1);
        return view('admin.policy.index', compact('policy'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'content' => 'required'
        ]);

        $policy = Policy::findOrFail(1);
        $policy->content = $request->content;
        $policy->update();
        
        return redirect(route('admin.policy'))->with('status', 'Policy content updated successfully');
    }
}
