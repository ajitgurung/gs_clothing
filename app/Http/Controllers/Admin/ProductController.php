<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\CropImg;
use App\Models\Brand;
use App\Models\SizeChart;
use App\Models\Size;
use App\Models\Variation;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('created_at', 'DESC')->get()->chunk(50);
        return view('admin.product.index', compact('products'));
    }

    public function status(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->id;
            $status = Product::findOrFail($id);
            if ($status->status === 1) {
                $status->status = 0;
            } else {
                $status->status = 1;
            }
            // $status->updated_by = auth()->user()->name;
            $status->update();
            $response = array('success' => 'Status Updated Successfully !!');
            return json_encode($response);
        }
    }

    public function featured(Request $request)
    {
        $id = $request->id;
        $status = Product::findOrFail($id);
        if ($status->featured === 1) {
            $status->featured = 0;
        } else {
            $status->featured = 1;
        }
        // $status->updated_by = auth()->user()->name;
        $status->update();
        $response = array('success' => 'Featured Updated Successfully !!');
        return json_encode($response);
    }

    public function create()
    {
        $charts = SizeChart::orderBy('order_item')->get();
        $brands = Brand::orderBy('order_item')->get();
        $categories = Category::orderBy('order_item')->get();
        return view('admin.product.form', compact('categories', 'brands', 'charts'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title' => 'required|max:191',
            'sku' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
            'size_chart_id' => 'required',
            'selling_price' => 'required',
            'image' => 'required'
        ]);

        $product = new Product();
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->size_chart_id = $request->size_chart_id;
        $product->title = $request->title;
        $product->sku = $request->sku;

        $slug = Product::createSlug($request->title, 'id', 0);
        $product->slug = $slug;

        if ($request->display) {
            $product->display = 1;
        }
        if ($request->featured) {
            $product->featured = 1;
        }
        $product->selling_price = $request->selling_price;
        $product->marked_price = $request->marked_price;
        $product->short_desc = $request->short_desc;
        $product->desc = $request->desc;

        $product->created_by = auth()->user()->name;
        $product->updated_by = auth()->user()->name;


        $path = public_path() . '/storage/product/' . $slug;
        $folderPath = 'public/product/' . $slug;
        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
        }

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,webp|max:50000',
            ]);
            $file = $request->file('image');
            $fileName = time() . '.webp';

            CropImg::resize_crop_images(480, 480, $file, $folderPath . "/" . $fileName);
            CropImg::resize_crop_images(85, 85, $file, $folderPath . "/small_" . $fileName);
            $product->image = $fileName;
        }

        if ($request->file('gallery_image')) {
            for ($i = 0; $i < count($request->file('gallery_image')); $i++) {
                $img = $request->file('gallery_image')[$i];
                $fileNameToStore = (string)$i . '_' . time() . '.webp';

                if (!file_exists($folderPath . '/gallery')) {
                    Storage::makeDirectory($folderPath . '/gallery', 0777, true, true);
                    Storage::makeDirectory($folderPath . '/gallery/thumbs', 0777, true, true);
                }

                // Storage::putFileAs($folderPath, new file($img), $fileNameToStore);
                CropImg::resize_crop_images(480, 480, $img, $folderPath . "/gallery/" . $fileNameToStore);
                CropImg::resize_crop_images(85, 85, $img, $folderPath . "/gallery/thumbs/" . $fileNameToStore);
                // CropImg::resize_crop_images(445, 605, $img, $folderPath . "/gallery/cover_" . $fileNameToStore);
            }
        }
        $product->save();
        return redirect(route('admin.product.index'))->with('status', 'Product created successfully');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $charts = SizeChart::orderBy('order_item')->get();
        $brands = Brand::orderBy('order_item')->get();
        $categories = Category::orderBy('order_item')->get();
        return view('admin.product.form', compact('product', 'categories', 'brands', 'charts'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:191',
            'sku' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
            'size_chart_id' => 'required',
            'selling_price' => 'required',
        ]);

        $product = Product::findOrFail(base64_decode($id));
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->size_chart_id = $request->size_chart_id;

        if ($product->title != $request->title) {
            $slug = Product::createSlug($request->title, $product->id);
            $product->title = $request->title;
        }

        $product->sku = $request->sku;

        if ($request->display) {
            $product->display = 1;
        }
        if ($request->featured) {
            $product->featured = 1;
        }
        $product->selling_price = $request->selling_price;
        $product->marked_price = $request->marked_price;
        $product->short_desc = $request->short_desc;
        $product->desc = $request->desc;

        $product->created_by = auth()->user()->name;
        $product->updated_by = auth()->user()->name;

        $path = public_path() . '/storage/product/' . $product->slug;
        if (isset($slug)) {
            if ($product->slug != $slug) {
                $oldslug = $product->slug;
                if (file_exists($path)) {
                    Storage::move('public/product/' . $oldslug, 'public/product/' . $slug);
                }
                $product->slug = $slug;
            }
        } else {
            $slug = $product->slug;
        }
        $folderPath = 'public/product/' . $slug;

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,webp|max:50000',
            ]);
            $oldfile = $product->image;
            $file = $request->file('image');
            $fileName = time() . '.webp';

            CropImg::resize_crop_images(480, 480, $file, $folderPath . "/" . $fileName);
            CropImg::resize_crop_images(85, 85, $file, $folderPath . "/small_" . $fileName);
            $product->image = $fileName;
            Storage::delete($folderPath . "/" . $oldfile);
            Storage::delete($folderPath . "/small_" . $oldfile);
        }

        if ($request->file('gallery_image')) {
            for ($i = 0; $i < count($request->file('gallery_image')); $i++) {
                $img = $request->file('gallery_image')[$i];
                $fileNameToStore = (string)$i . '_' . time() . '.webp';

                if (!file_exists($folderPath . '/gallery')) {
                    Storage::makeDirectory($folderPath . '/gallery', 0777, true, true);
                    Storage::makeDirectory($folderPath . '/gallery/thumbs', 0777, true, true);
                }

                // Storage::putFileAs($folderPath, new file($img), $fileNameToStore);
                CropImg::resize_crop_images(480, 480, $img, $folderPath . "/gallery/big_" . $fileNameToStore);
                CropImg::resize_crop_images(85, 85, $img, $folderPath . "/gallery/thumbs/" . $fileNameToStore);
                // CropImg::resize_crop_images(445, 605, $img, $folderPath . "/gallery/cover_" . $fileNameToStore);
            }
        }

        $product->update();
        return redirect(route('admin.product.index'))->with('status', 'Product updated successfully');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        Storage::deleteDirectory('public/product/' . $product->slug);
        $product->delete();
        return redirect()->back()->with('status', 'Product deleted successfully');
    }

    public function delete_gallery_image($albumName, $photoName)
    {
        // Storage::delete("public/product/" . $albumName . "/gallery/cover_" . $photoName);
        // Storage::delete("public/product/" . $albumName . "/gallery/" . $photoName);
        Storage::delete("public/product/" . $albumName . "/gallery/" . $photoName);
        Storage::delete("public/product/" . $albumName . "/gallery/thumbs/" . $photoName);
        return redirect()->back()->with('status', 'Gallery Image Deleted Successfully!');
    }


    public function variation($slug)
    {
        $variations = Variation::all();
        return view('admin.product.variation', compact('slug', 'variations'));
    }

    public function addVariationForm($slug)
    {
        $sizes = Size::orderBy('order_item')->get();
        return view('admin.product.variation-form', compact('slug', 'sizes'));
    }

    public function addVariation(Request $request, $slug)
    {
        $this->validate($request, [
            'title' => 'required',
            'color_code' => 'required',
            'image' => 'required'
        ]);

        $product = Product::where('slug', $slug)->firstOrFail();

        $check = Variation::where('product_id', $product->id)->where('color_code', $request->color_code)->first();
        if ($check !== null) {
            return back()->with('error', 'Color already exists');
        }

        $variation = new Variation();
        $variation->product_id = $product->id;
        $variation->title = $request->title;
        $variation->color_code = $request->color_code;
        $variation->sizes = json_encode($request->sizes);

        $path = public_path() . '/storage/product/' . $slug . '/' . substr($request->color_code, 1);
        $folderPath = 'public/product/' . $slug . '/' . substr($request->color_code, 1);
        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
        }

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,webp|max:50000',
            ]);
            $file = $request->file('image');
            $fileName = time() . '.webp';

            CropImg::resize_crop_images(480, 480, $file, $folderPath . "/" . $fileName);
            CropImg::resize_crop_images(85, 85, $file, $folderPath . "/small_" . $fileName);
            $variation->image = $fileName;
        }

        $gallery = [];

        if ($request->file('gallery_image')) {
            for ($i = 0; $i < count($request->file('gallery_image')); $i++) {
                $img = $request->file('gallery_image')[$i];
                $fileNameToStore = (string)$i . '_' . time() . '.webp';

                if (!file_exists($folderPath . '/gallery')) {
                    Storage::makeDirectory($folderPath . '/gallery', 0777, true, true);
                    Storage::makeDirectory($folderPath . '/gallery/Thumbs/', 0777, true, true);
                }

                array_push($gallery, $fileNameToStore);

                // Storage::putFileAs($folderPath, new file($img), $fileNameToStore);
                CropImg::resize_crop_images(480, 480, $img, $folderPath . "/gallery/" . $fileNameToStore);
                CropImg::resize_crop_images(85, 85, $img, $folderPath . "/gallery/thumbs/" . $fileNameToStore);
                // CropImg::resize_crop_images(445, 605, $img, $folderPath . "/gallery/cover_" . $fileNameToStore);
            }
        }

        $variation->gallery_images = json_encode($gallery);
        $variation->save();

        return redirect(route('admin.product.variation.index', $slug))->with('status', 'Variation added successfully');
    }

    public function editVariation($slug, $id)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        $variation = Variation::findOrFail(base64_decode($id));
        $sizes = Size::orderBy('order_item')->get();
        return view('admin.product.variation-form', compact('slug', 'sizes', 'product', 'variation'));
    }

    public function updateVariation(Request $request, $slug, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'color_code' => 'required',
        ]);

        $product = Product::where('slug', $slug)->firstOrFail();

        $variation = Variation::findOrFail(base64_decode($id));
        $variation->product_id = $product->id;
        $variation->title = $request->title;
        $variation->sizes = json_encode($request->sizes);

        $path = public_path() . '/storage/product/' . $slug . '/' . substr($variation->color_code, 1);
        if ($variation->color_code != $request->color_code) {
            if (file_exists($path)) {
                Storage::move('public/product/' . $slug . '/' . substr($variation->color_code, 1), 'public/product/' . $slug . '/' . substr($request->color_code, 1));
            }
            $variation->color_code = $request->color_code;
        }
        $folderPath = 'public/product/' . $slug . '/' . substr($variation->color_code, 1);

        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
        }

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,webp|max:50000',
            ]);
            $file = $request->file('image');
            $fileName = time() . '.webp';

            CropImg::resize_crop_images(480, 480, $file, $folderPath . "/" . $fileName);
            CropImg::resize_crop_images(85, 85, $file, $folderPath . "/small_" . $fileName);
            $variation->image = $fileName;
        }

        $gallery = [];

        if ($request->file('gallery_image')) {
            for ($i = 0; $i < count($request->file('gallery_image')); $i++) {
                $img = $request->file('gallery_image')[$i];
                $fileNameToStore = (string)$i . '_' . time() . '.webp';

                if (!file_exists($folderPath . '/gallery')) {
                    Storage::makeDirectory($folderPath . '/gallery', 0777, true, true);
                    Storage::makeDirectory($folderPath . '/gallery/thumbs', 0777, true, true);
                }

                array_push($gallery, $fileNameToStore);

                // Storage::putFileAs($folderPath, new file($img), $fileNameToStore);
                CropImg::resize_crop_images(480, 480, $img, $folderPath . "/gallery/" . $fileNameToStore);
                CropImg::resize_crop_images(85, 85, $img, $folderPath . "/gallery/thumbs/" . $fileNameToStore);
                // CropImg::resize_crop_images(445, 605, $img, $folderPath . "/gallery/cover_" . $fileNameToStore);
            }
        }

        $variation->gallery_images = json_encode($gallery);
        $variation->save();

        return redirect(route('admin.product.variation.index', $slug))->with('status', 'Variation added successfully');
    }

    public function deleteVariation($slug, $id)
    {
        $variation = Variation::findOrFail(base64_decode($id));
        $path = public_path() . '/storage/product/' . $slug . '/' . substr($variation->color_code, 1);
        $folderPath = 'public/product/' . $slug . '/' . substr($variation->color_code, 1);
        if (file_exists($path)) {
            Storage::deleteDirectory($folderPath);
        }
        $variation->delete();
        return back()->with('status', 'Variation deleted successfully');
    }

    public function delete_variation_image($id, $photoName)
    {
        $variation = Variation::findOrFail($id);
        // Storage::delete("public/product/" . $albumName . "/gallery/big_" . $photoName);
        // Storage::delete("public/product/" . $albumName . "/gallery/cover_" . $photoName);
        Storage::delete("public/product/" . $variation->product->slug . "/" . substr($variation->color_code, 1) . '/gallery/' . $photoName);
        Storage::delete("public/product/" . $variation->product->slug . "/" . substr($variation->color_code, 1) . '/gallery/thumbs/' . $photoName);
        return redirect()->back()->with('status', 'Gallery Image Deleted Successfully!');
    }
}
