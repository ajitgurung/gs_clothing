<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Models\CropImg;
use Intervention\Image\Facades\Image;
use Illuminate\Http\File;

class CategoryController extends Controller
{
    public function index()
    {
        $category = null;
        $categories = $this->getFullListFromDB();

        return view('admin.category.index', array('categories' => $categories, 'category' => $category, 'id' => '0'));
    }


    public function getFullListFromDB($parent_id = 0)
    {
        $categories = DB::table('categories')->orderBy('order_item')->where('parent_id', $parent_id)->get();
        foreach ($categories as $value) {
            $subresult = $this->getFullListFromDB($value->id);
            if (count($subresult) > 0) {
                $value->children = $subresult->all();
            }
        }
        unset($value);

        return $categories;
    }

    public function set_order(Request $request)
    {
        $categories = new Category();
        $list_order = $request['list_order'];

        $this->saveList($list_order, $request->parentid);
        $data = array('status' => 'success');
        echo json_encode($data);
        exit;
    }

    function saveList($list, $parent_id = 0, $child = 0, &$m_order = 0)
    {
        foreach ($list as $item) {
            $m_order++;
            $updateData = array("parent_id" => $parent_id, "child" => $child, "order_item" => $m_order);
            Category::where('id', $item['id'])->update($updateData);

            if (array_key_exists("children", $item)) {
                $updateData = array("child" => 1);
                Category::where('id', $item['id'])->update($updateData);
                $this->saveList($item["children"], $item['id'], 0, $m_order);
            }
        }
    }

    public function store(Request $request){
        $this->validate($request, [
            'title' => 'required|max:255'
        ]);
        $category = new Category();
        $category->title = $request->title;

        $slug = Category::createSlug($request->title, 'id', 0);
        $category->slug = $slug;

        $max_order = DB::table('categories')->max('order_item');
        $category->order_item = $max_order + 1;

        if($request->status){
            $category->status = 1;
        }

        if($request->featured){
            $category->featured = 1;
        }

        if ($request->hasFile('image')) {
            $path = public_path() . '/storage/category/' . $slug;
            $folderPath = 'public/category/' . $slug;

            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,svg|max:50000',
            ]);
            $file = $request->file('image');
            $fileName = time() . '.webp';
            
            Storage::putFileAs($folderPath, new File($file), $fileName);
            $category->image = $fileName;
        }

        $category->save();
        return redirect(route('admin.category.index'))->with('status', 'Category created successfully!');
    }

    public function edit($id){
        $category = Category::findOrFail(base64_decode($id));
        return view('admin.category.edit')->with('category', $category);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'title' => 'required'
        ]);

        $category = Category::findOrFail(base64_decode($id));
        if($category->title != $request->title){
            $slug = Category::createSlug($request->title, 'id', 0);
        }

        $category->title = $request->title;

        if($request->status){
            $category->status = 1;
        }else{
            $category->status = 0;
        }

        if($request->featured){
            $category->featured = 1;
        }else{
            $category->featured = 0;
        }

        $path = public_path() . '/storage/category/' . $category->slug;
        if(isset($slug)){
            if ($category->slug != $slug) {
                $oldslug = $category->slug;
                $category->slug = $slug;
                if (file_exists($path)) {
                    Storage::move('public/category/' . $oldslug, 'public/category/' . $slug);
                }
            }
        }else{
            $slug = $category->slug;
            $path = public_path() . '/storage/category/' . $slug;
        }
        $folderPath = 'public/category/' . $slug;

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,svg|max:50000',
            ]);
            $oldImage = $category->image;

            $file = $request->file('image');
            $fileName = time() . '.'. $file->getClientOriginalExtension();
            Storage::putFileAs($folderPath, new File($file), $fileName);

            $category->image = $fileName;
            if($oldImage !== null || $oldImage !== ''){
                Storage::delete('public/category/' . $slug . '/' . $oldImage);
            }
        }


        $category->update();
        return redirect(route('admin.category.index'))->with('status', 'Category updated successfully!');
    }

    public function destroy($id){
        $category = Category::findOrFail(base64_decode($id));
        Storage::deleteDirectory('public/category/'.$category->slug);
        $category->delete();
        return redirect()->back()->with('status', 'Category and products deleted successfully!');
    }
}
