<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use Spatie\Permission\Models\Role;
use Hash;
use Illuminate\Support\Arr;
use Illuminate\Auth\Events\Verified;

class UserController extends Controller
{
    public function index()
    {
        $data = User::orderBy('id','DESC')->where('id', '!=', 1)->where('role', 1)->paginate(10);
        return view('admin.users.index',compact('data'));
    }
    
    public function customer(){
        $data = User::orderBy('id','DESC')->where('id', '!=', 1)->where('role', 0)->paginate(10);
        return view('admin.users.index',compact('data'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function show($id){
        $user = User::findOrFail($id);
        return view('admin.users.show', compact('user'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password'
        ]);


        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->role = 1;
        $user->save();
        
        return redirect()->route('admin.users.index')->with('status','User updated successfully');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit',compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'same:confirm-password'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,['password']);
        }

        $user = User::findOrFail($id);
        if($user->id === 1){
            return redirect()->back()->with('error', 'Not authorized');
        }
        $user->update($input);

        return redirect()->route('admin.users.index')->with('status','User updated successfully');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($user->id === 1){
            return redirect()->back()->with('error', 'Not authorized');
        }
        $user->delete();
        return redirect()->route('admin.users.index')
            ->with('status','User deleted successfully');
    }
}
