<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{
    public function index()
    {
        $items = Faq::orderBy('order_item')->get();
        return view('admin.faq.index', compact('items'));
    }

    public function set_order(Request $request)
    {
        $list_order = [];
        $list_order = $request['list_order'];
        $i = 1;
        foreach ($list_order as $id) {
            $updateData = array("order_item" => $i);
            Faq::where('id', $id)->update($updateData);
            $i++;
        }
        $data = array('status' => 'success');
        return json_encode($data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',
            'answer' => 'required',
        ]);

        $item = new Faq();
        $item->question = $request->question;

        $max_order = DB::table('faqs')->max('order_item');
        $item->order_item = $max_order + 1;

        $item->answer = $request->answer;

        if ($request->status) {
            $item->status = 1;
        }

        $item->save();
        return redirect(route('admin.faq.index'))->with('status', 'Faq created successfully');
    }

    public function edit($id)
    {
        $item = Faq::findOrFail(base64_decode($id));
        return view('admin.faq.form', compact('item'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'question' => 'required',
            'answer' => 'required',
        ]);

        $item = Faq::findOrFail(base64_decode($id));

        $item->question = $request->question;
        $item->answer = $request->answer;

        if ($request->status) {
            $item->status = 1;
        } else {
            $item->status = 0;
        }
        $item->update();
        return redirect(route('admin.faq.index'))->with('status', 'Faq content updated successfully');
    }

    public function destroy($id)
    {
        $item = Faq::firstOrFail(base64_decode($id));
        $item->delete();
        return redirect()->back()->with('status', 'Faq content deleted successfully!');
    }
}
