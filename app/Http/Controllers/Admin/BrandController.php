<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class BrandController extends Controller
{
    public function index(){
        $brands = Brand::orderBy('order_item')->get();
        return view('admin.brand.index', compact('brands'));
    }
    
    public function set_order(Request $request)
    {
        $list_order = [];
        $list_order = $request['list_order'];
        $i = 1;
        foreach ($list_order as $id) {
            $updateData = array("order_item" => $i);
            Brand::where('id', $id)->update($updateData);
            $i++;
        }
        $data = array('status' => 'success');
        echo json_encode($data);
    }

    public function create(){
        return view('admin.brand.form');
    }

    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required'
        ]);

        $brand = new Brand();
        $brand->title = $request->title;
        $slug = Brand::createSlug($request->title, 'id', 0);
        $brand->slug = $slug;
        $max_order = DB::table('brands')->max('order_item');
        $brand->order_item = $max_order + 1;
        $path = public_path() . '/storage/brand/';
        $folderPath = 'public/brand/';
        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
        }

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg|max:50000',
            ]);
            $file = $request->file('image');
            $fileName = time() . '.webp';
            
            $cover = storage_path('app/' . $folderPath. $fileName);
            Image::make($file)->encode('webp')->save($cover);
            // Storage::putFileAs($folderPath, new file($file), $fileName);
            $brand->image = $fileName;
        }
        $brand->save();
        // for($i=1; $i < 5; $i++){
        //     $banner = new BrandBanner();
        //     $banner->brand_id = $brand->id;
        //     $banner->title = $i;
        //     $banner->order_item = $i;
        //     $banner->save();
        // }
        return redirect(route('admin.brand.index'))->with('status', 'Brand created successfully');
    }

    public function edit($id){
        $brand = Brand::findOrFail(base64_decode($id));
        return view('admin.brand.form')->with('brand', $brand);
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'title' => 'required'
        ]);

        $brand = Brand::findOrFail(base64_decode($id));
        if($brand->title != $request->title){
            $slug = Brand::createSlug($request->title, 'id', 0);
        }else{
            $slug = $brand->slug;
        }
        $brand->title = $request->title;
        $brand->slug = $slug;
        $path = public_path() . '/storage/brand/';
        $folderPath = 'public/brand/';
        if (!file_exists($path)) {
            Storage::makeDirectory($folderPath, 0777, true, true);
        }

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg|max:50000',
            ]);
            $oldImage = $brand->image;
            $file = $request->file('image');
            $fileName = time() . '.webp';
            
            $cover = storage_path('app/' . $folderPath. $fileName);
            Image::make($file)->encode('webp')->save($cover);

            // Storage::putFileAs($folderPath, new file($file), $fileName);
            $brand->image = $fileName;
            if(isset($oldImage)){
                Storage::delete('public/brand/' . $oldImage);
            }
        }
        $brand->update();
        return redirect(route('admin.brand.index'))->with('status', 'Brand updated successfully');
    }

    public function destroy($id){
        $brand = Brand::findOrFail(base64_decode($id));
        if(isset($brand->image)){
            Storage::delete('public/brand/' . $brand->image);
        }

        $brand->delete();
        return redirect()->back()->with('status', 'Brand deleted successfully');
    }
}
