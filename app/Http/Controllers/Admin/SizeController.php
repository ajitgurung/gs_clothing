<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Size;
use Illuminate\Support\Facades\DB;

class SizeController extends Controller
{
    public function index()
    {
        $sizes = Size::orderBy('order_item')->get();
        return view('admin.size.index', compact('sizes'));
    }

    public function set_order(Request $request)
    {    
        $list_order = [];
        $size = new Size;

        $list_order = $request['list_order'];
        $i = 1;
        foreach ($list_order as $id) {
            $updateData = array("order_item" => $i);
            Size::where('id', $id)->update($updateData);
            $i++;
        }
        $data = array('status' => 'success');
        return json_encode($data);
    }
    

    public function store(Request $request)
    {
        $this->validate($request, [
            "title" => 'required|max:255',
            "code" => 'required|max:255'
        ]);
        $size = new Size();
		$max_order = DB::table('sizes')->max('order_item');
		$size->title = $request->title;
		$size->code = $request->code;
        $size->order_item = $max_order + 1;

		$size->created_by = auth()->user()->name;
		$size->updated_by = auth()->user()->name;

		$size->save();

        return redirect(route('admin.size.index'))->with('status', 'Size Added Successfully!');
    }

    public function edit($id)
    {
        $size = Size::findOrFail(base64_decode($id));
        return view('admin.size.edit', compact('size'));
    }

    public function update(SizeRequest $request, $id)
    {       
        $this->validate($request, [
            "title" => 'required|max:255',
            "code" => 'required|max:255'
        ]);
        $size = Size::findOrFail(base64_decode($id));
        $size->title = $request->title;
        $size->code = $request->code;

		$size->updated_by = auth()->user()->name;
  
        $size->update();
    	return redirect(route('admin.size.index'))->with('status', 'Size Updated Successfully!');
    }


    public function destroy($id)
    {
        $size = Size::findOrFail(base64_decode($id));      
        $size->delete();

        return redirect()->back()->with('status', 'Size Deleted Successfully!');
    }
}
