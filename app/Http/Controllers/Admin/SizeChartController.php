<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SizeChart;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

class SizeChartController extends Controller
{
    public function index()
    {
        $sizecharts = SizeChart::orderBy('order_item')->get();
        return view('admin.sizechart.index', compact('sizecharts'));
    }

    public function set_order(Request $request)
    {
        $list_order = [];
        $sizechart = new SizeChart;

        $list_order = $request['list_order'];
        $i = 1;
        foreach ($list_order as $id) {
            $updateData = array("order_item" => $i);
            SizeChart::where('id', $id)->update($updateData);
            $i++;
        }
        $data = array('status' => 'success');
        return json_encode($data);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            "title" => 'required|max:255',
            'filename' => 'required|mimes:pdf|max:2048'
        ]);
        $sizechart = new SizeChart();
        $max_order = DB::table('size_charts')->max('order_item');
        $sizechart->title = $request->title;
        $sizechart->order_item = $max_order + 1;

        $path = public_path() . '/storage/size-charts/';
        $folderPath = 'public/size-charts/';

        // dd($request);
        if ($request->hasFile('filename')) {
            $file = $request->file('filename');
            $validatedData = $request->validate([
                'filename' => 'file|mimes:pdf|max:10000',
            ]);

            $filename = time() . '.' . $file->getClientOriginalExtension();
            $folderPath = "public/size-charts";

            Storage::putFileAs($folderPath, new File($file), $filename);
            $sizechart->filename = $filename;
        }


        $sizechart->created_by = auth()->user()->name;
        $sizechart->updated_by = auth()->user()->name;

        $sizechart->save();

        return redirect(route('admin.sizechart.index'))->with('status', 'Size Chart Added Successfully!');
    }

    public function edit($id)
    {
        $sizechart = SizeChart::findOrFail(base64_decode($id));
        return view('admin.sizechart.edit', array('sizechart' => $sizechart, 'id' => base64_decode($id)));
    }

    public function update(SizeChartRequest $request, $id)
    {
        $this->validate($request, [
            "title" => 'required|max:255',
            'filename' => 'mimes:pdf|max:2048'
        ]);
        $sizechart = SizeChart::findOrFail(base64_decode($id));
        $sizechart->title = $request->title;

        $path = public_path() . '/storage/size-charts/';
        $folderPath = 'public/size-charts/';

        if ($request->hasFile('filename')) {
            $oldfilename = $sizechart->filename;
            $file = $request->file('filename');

            $filename = time() . '.' . $file->getClientOriginalExtension();

            Storage::putFileAs($folderPath, new File($file), $filename);

            $sizechart->filename = $filename;

            Storage::delete($folderPath . '/' . $oldfilename);
        }

        $sizechart->updated_by = auth()->user()->name;

        $sizechart->update();
        return redirect(route('admin.sizechart.index'))->with('status', 'Size Chart Updated Successfully!');;
    }


    public function destroy($id)
    {
        $sizechart = SizeChart::findOrFail(base64_decode($id));

        // $productExists = Product::where('size_chart_id',$sizechart->id)->exists();

        // if ($productExists) {
        //     return redirect()->back()->with('status','This Size Chart is used in Products!');
        // }

        $oldfile = $sizechart->filename;
        $folderPath = 'public/size-charts/';

        Storage::delete($folderPath . '/' . $oldfile);
        $sizechart->delete();

        return redirect()->back()->with('status', 'Size Chart Deleted Successfully!');
    }
}
