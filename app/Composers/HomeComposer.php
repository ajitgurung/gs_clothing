<?php

namespace App\Composers;

use App\Models\Setting;

class HomeComposer
{

    public function compose($view)
    {
        $setting = Setting::first();
        $view->with('setting', $setting);
    }
}
