<!-- Content -->
<div class="newsletter v3">
    <div class="container container-content">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="newsletter-heading">
                    <h3 class="d-flex"><i class="ri-arrow-right-line"></i>get in touch</h3>
                    <p>Our Customer Suppot is always ready to help you.</p>
                </div>
                <div class="contact-element">
                    <p><a href=""><i class="ri-mail-open-line"></i>&nbsp;&nbsp;support@gscloth-land.com</a></p>
                    <p><a href=""><i class="ri-phone-line"></i>&nbsp;&nbsp;01-4-554444</a></p>
                    <p><a href=""><i class="ri-smartphone-line"></i>&nbsp;&nbsp;+977-9800000000</a></p>
                </div>
                <div class="social">
                    <a href=""><i class="ri-facebook-fill"></i></a>
                    <a href=""><i class="ri-instagram-line"></i></a>
                    <a href=""><i class="ri-whatsapp-line"></i></a>
                </div>

            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="newsletter-flex">
                    <div class="newsletter-element">
                        <h3>Quick Links</h3>
                        <ul>
                            <li><a href="">FAQs</a></li>
                            <li><a href="">Contact Us </a></li>
                            <li><a href="">Gallery</a></li>
                            <li><a href="">Our Blog</a></li>
                        </ul>
                    </div>
                    <div class="newsletter-element">
                        <h3>shopping</h3>
                        <ul>
                            <li><a href="">Mens</a></li>
                            <li><a href="">Womens</a></li>
                            <li><a href="">Kids</a></li>
                            <li><a href="">Unisex</a></li>
                        </ul>
                    </div>
                    <div class="newsletter-element">
                        <h3>My Account</h3>
                        <ul>
                            <li><a href="">My Cart</a></li>
                            <li><a href="">Login</a></li>
                            <li><a href="">Register</a></li>
                            <li><a href="">My Account</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- EndContent -->
<!-- Footer -->
<footer class="footer v2">
    <div class="container">
        <div class="f-content">
            <div class="f-col ">
                <p>© 20122 All Rights Reserved. <a class="" href="">GS CLothLand.</a></p>
            </div>
            <div class="f-col align-items-center text-center" style="justify-content:center">

                <ul>
                    <li><a href="">Return Policy</a></li>
                    <li><a href="">Terms of Use</a></li>
                </ul>
            </div>
            <div class="f-col">
                <p>Coded with <i class="ri-heart-fill"></i> by <a class="" href="">KTMRush</a></p>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->