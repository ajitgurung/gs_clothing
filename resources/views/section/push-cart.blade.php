<div class="pushmenu pushmenu-left cart-box-container">
    <div class="cart-list">
        <div class="cart-list-heading">
            <h3 class="cart-title">My cart</h3>
            <span class="close-left js-close"><i class="ri-close-line"></i></span>
        </div>
        <div class="cart-inside">
            <ul class="list">
                <li class="item-cart">
                    <div class="product-img-wrap">
                        <a href="#" title="Product"><img src="img/product/cart_product_1.jpg" alt="Product"
                                class="img-responsive"></a>
                    </div>
                    <div class="product-details">
                        <div class="inner-left">
                            <div class="product-name"><a href="#">Grosgrain tie cotton top</a></div>
                            <div class="product-price"><span>Rs. 1925</span></div>
                            <div class="cart-qtt">
                                <button type="button" class="quantity-left-minus btn btn-number js-minus"
                                    data-type="minus" data-field="">
                                    <span class="minus-icon"><i class="ri-subtract-line"></i></span>
                                </button>
                                <input type="text" name="number" value="1"
                                    class="product_quantity_number js-number">
                                <button type="button" class="quantity-right-plus btn btn-number js-plus"
                                    data-type="plus" data-field="">
                                    <span class="plus-icon"><i class="ri-add-line"></i></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="item-cart">
                    <div class="product-img-wrap">
                        <a href="#" title="Product"><img src="img/product/cart_product_2.jpg" alt="Product"
                                class="img-responsive"></a>
                    </div>
                    <div class="product-details">
                        <div class="inner-left">
                            <div class="product-name"><a href="#">Grosgrain tie cotton top</a></div>
                            <div class="product-price"><span>Rs. 1925</span></div>
                            <div class="cart-qtt">
                                <button type="button" class="quantity-left-minus btn btn-number js-minus"
                                    data-type="minus" data-field="">
                                    <span class="minus-icon"><i class="ri-subtract-line"></i></span>
                                </button>
                                <input type="text" name="number" value="1"
                                    class="product_quantity_number js-number">
                                <button type="button" class="quantity-right-plus btn btn-number js-plus"
                                    data-type="plus" data-field="">
                                    <span class="plus-icon"><i class="ri-add-line"></i></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="cart-bottom">

                <div class="cart-button mg-top-30">
                    <a class="zoa-btn checkout" href="#" title="">Check out</a>
                </div>
            </div>
        </div>
        <!-- End cart bottom -->
    </div>
</div>