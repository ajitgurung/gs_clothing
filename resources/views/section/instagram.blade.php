<div class="container container-content">
    <div class="zoa-instagram">
        <div class="insta-title2 text-center">
            <h3>INSTAGRAM</h3>
            <a href="">@GSClothLand</a>
        </div>
        <div class="row insta-content">
            <div class="col-md-2 col-sm-4 col-xs-6">
                <a href=""><img src="{{ asset('img/home4/insta_1.jpg') }}" alt=""
                        class="img-responsive"></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <a href=""><img src="{{ asset('img/home4/insta_2.jpg') }}" alt=""
                        class="img-responsive"></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <a href=""><img src="{{ asset('img/home4/insta_3.jpg') }}" alt=""
                        class="img-responsive"></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <a href=""><img src="{{ asset('img/home4/insta_4.jpg') }}" alt=""
                        class="img-responsive"></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <a href=""><img src="{{ asset('img/home4/insta_5.jpg') }}" alt=""
                        class="img-responsive"></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <a href=""><img src="{{ asset('img/home4/insta_6.jpg') }}" alt=""
                        class="img-responsive"></a>
            </div>
        </div>
    </div>
</div>