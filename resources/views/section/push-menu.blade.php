<div class="pushmenu menu-home5">
    <div class="menu-push">
        <span class="close-left js-close"><i class="ion-ios-close-empty f-40"></i></span>
        <div class="clearfix"></div>
        <form role="search" method="get" id="searchform" class="searchform" action="">
            <div>
                <label class="screen-reader-text" for="q"></label>
                <input type="text" placeholder="Search for products" value="" name="q" id="q" autocomplete="off">
                <input type="hidden" name="type" value="product">
                <button type="submit" id="searchsubmit"><i class="ion-ios-search-strong"></i></button>
            </div>
        </form>
        <ul class="nav-home5 js-menubar">
            <li class="active">
                <a href="#">Home</a>
            </li>
            <li class="level1 active dropdown"><a href="#">Men</a>
                <span class="icon-sub-menu"></span>
                <div class="menu-level1 js-open-menu">
                    <ul class="level1">
                        <li class="level2">
                            <a href="#">Tops</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Shirts</a></li>
                                <li class="level3"><a href="#" title="">Sweaters</a></li>
                                <li class="level3"><a href="#" title="">T-Shirts</a></li>
                                <li class="level3"><a href="#" title="">Jackets</a></li>
                                <li class="level3"><a href="#" title="">Hoodies</a></li>
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Bottoms</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Jeans</a></li>
                                <li class="level3"><a href="#" title="">Shorts</a></li>
                                <li class="level3"><a href="#" title="">Joggers</a></li>
                                <li class="level3"><a href="#" title="">Trousers</a></li>
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Accessories</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Belts</a></li>
                                <li class="level3"><a href="#" title="">Tie</a></li>

                                <li class="level3"><a href="#" title="">Wrist Bands</a></li>
                                <li class="level3"><a href="#" title="">Scarfs</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </li>
            <li class="level1 active dropdown"><a href="#">Women</a>
                <span class="icon-sub-menu"></span>
                <div class="menu-level1 js-open-menu">
                    <ul class="level1">
                        <li class="level2">
                            <a href="#">Tops</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Shirts</a></li>
                                <li class="level3"><a href="#" title="">Sweaters</a></li>
                                <li class="level3"><a href="#" title="">T-Shirts</a></li>
                                <li class="level3"><a href="#" title="">Jackets</a></li>
                                <li class="level3"><a href="#" title="">Hoodies</a></li>
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Bottoms</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Jeans</a></li>
                                <li class="level3"><a href="#" title="">Shorts</a></li>
                                <li class="level3"><a href="#" title="">Joggers</a></li>
                                <li class="level3"><a href="#" title="">Trousers</a></li>
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Accessories</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Belts</a></li>
                                <li class="level3"><a href="#" title="">Tie</a></li>

                                <li class="level3"><a href="#" title="">Wrist Bands</a></li>
                                <li class="level3"><a href="#" title="">Scarfs</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </li>
            <li class="level1 active dropdown"><a href="#">Kids</a>
                <span class="icon-sub-menu"></span>
                <div class="menu-level1 js-open-menu">
                    <ul class="level1">
                        <li class="level2">
                            <a href="#">Tops</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Shirts</a></li>
                                <li class="level3"><a href="#" title="">Sweaters</a></li>
                                <li class="level3"><a href="#" title="">T-Shirts</a></li>
                                <li class="level3"><a href="#" title="">Jackets</a></li>
                                <li class="level3"><a href="#" title="">Hoodies</a></li>
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Bottoms</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Jeans</a></li>
                                <li class="level3"><a href="#" title="">Shorts</a></li>
                                <li class="level3"><a href="#" title="">Joggers</a></li>
                                <li class="level3"><a href="#" title="">Trousers</a></li>
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Accessories</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Belts</a></li>
                                <li class="level3"><a href="#" title="">Tie</a></li>

                                <li class="level3"><a href="#" title="">Wrist Bands</a></li>
                                <li class="level3"><a href="#" title="">Scarfs</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </li>
            <li class="level1 active dropdown"><a href="#">Unisex</a>
                <span class="icon-sub-menu"></span>
                <div class="menu-level1 js-open-menu">
                    <ul class="level1">
                        <li class="level2">
                            <a href="#">Tops</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Shirts</a></li>
                                <li class="level3"><a href="#" title="">Sweaters</a></li>
                                <li class="level3"><a href="#" title="">T-Shirts</a></li>
                                <li class="level3"><a href="#" title="">Jackets</a></li>
                                <li class="level3"><a href="#" title="">Hoodies</a></li>
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Bottoms</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Jeans</a></li>
                                <li class="level3"><a href="#" title="">Shorts</a></li>
                                <li class="level3"><a href="#" title="">Joggers</a></li>
                                <li class="level3"><a href="#" title="">Trousers</a></li>
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Accessories</a>
                            <ul class="menu-level-2">
                                <li class="level3"><a href="#" title="">Belts</a></li>
                                <li class="level3"><a href="#" title="">Tie</a></li>

                                <li class="level3"><a href="#" title="">Wrist Bands</a></li>
                                <li class="level3"><a href="#" title="">Scarfs</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </li>
            <li class="level1">
                <a href="#">About Us</a>
            </li>
            <li class="level1">
                <a href="#">Contact Us</a>
            </li>
        </ul>
        <ul class="mobile-account">
            <li><a href=""><i class="fa fa-unlock-alt"></i>Login</a></li>
            <li><a href=""><i class="fa fa-user-plus"></i>Register</a></li>
            <li><a href=""><i class="fa fa-heart"></i>Wishlist</a></li>
        </ul>
        <h4 class="mb-title">connect and follow</h4>
        <div class="mobile-social mg-bottom-30">
            <a href=""><i class="fa fa-facebook"></i></a>
            <a href=""><i class="fa fa-twitter"></i></a>
            <a href=""><i class="fa fa-google-plus"></i></a>
        </div>
    </div>
</div>