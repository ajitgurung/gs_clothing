@extends('layouts.app')

@push('post-styles')
    <style>
        #header {
            border: 1px solid #eaeaea;
        }
    </style>
@endpush
@section('content')
    <div class="shop-heading text-center">
        <h1>New Arrivals</h1>
        <ul class="breadcrumb text-center">
            <li><a href="#">Home</a></li>
            <li class="active"><i class="ri-arrow-right-s-line"></i>&nbsp;New Arrivals</li>
        </ul>
    </div>

    <div class="container container-content" style="padding-top:40px;">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 col-left collection-sidebar" id="filter-sidebar">
                <div class="close-sidebar-collection hidden-lg hidden-md">
                    <span>Filter</span><i class="icon_close ion-close"></i>
                </div>
                @if ($categories->count() > 0)
                    <div class="widget-filter filter-cate no-pd-top">
                        <h3>Categories</h3>
                        <ul>
                            @foreach ($categories as $category)
                                <li>
                                     {{-- <a href="JavaScript:void(0);"> --}}
                                    <input type="radio" name="category_filter"
                                            value="{{ base64_encode($category->id) }}">   {{ $category->title }}
                                        {{-- </a> --}}

                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="widget-filter filter-cate filter-color">
                    <h3>Filter by colors</h3>
                    <ul>
                        <li><a class="active" href="">Black</a></li>
                        <li><a href="">White</a></li>
                        <li><a href="">Grey</a></li>
                    </ul>
                </div>
                @if ($sizes->count() > 0)
                    <div class="widget-filter filter-cate filter-size">
                        <h3>Filter by sizes</h3>
                        <ul>
                            @foreach ($sizes as $size)
                                <li>
                                    {{-- <a href="JavaScript:void(0);"> --}}
                                    <input type="radio" name="size_filter"
                                            value="{{ base64_encode($size->id) }}">   {{ $size->code }}
                                        {{-- </a> --}}
                                    </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if ($brands->count() > 0)
                    <div class="widget-filter filter-cate filter-size">
                        <h3>Filter by brand</h3>
                        <ul>
                            @foreach ($brands as $brand)
                                <li>
                                    {{-- <a href="JavaScript:void(0);"> --}}
                                    <input type="radio" name="brand_filter"
                                            value="{{ base64_encode($brand->id) }}">   {{ $brand->title }}
                                        {{-- </a> --}}
                                    </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <a class="zoa-btn btn-filter">
                    Reset Filter
                </a>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12 collection-list">
                <div class="product-collection-grid product-grid bd-bottom">
                    <div class="shop-top">
                        <div class="shop-element left">
                            <h1 class="shop-title">New Arrivals</h1>
                        </div>
                        <div class="filter-collection-left text-center hidden-lg hidden-md">
                            <a class="btn"><i class="zoa-icon-filter"></i> Filter</a>
                        </div>
                        <div class="shop-element left right">
                            <ul class="js-filter">
                                <li class="filter">
                                    <a onClick="return false;" href=""><i class="zoa-icon-sort"
                                            style="float:right"></i>Sort by:
                                        <span>Best selling</span> <i class="ri-arrow-down-s-line"
                                            style="float:right"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Featured</a></li>
                                        <li><a href="#ng">Best Selling</a></li>
                                        <li><a href="#ng">Alphabetically, A-Z</a></li>
                                        <li><a href="#ng">Alphabetically, A-Z</a></li>
                                        <li><a href="#ng">Price, high to low</a></li>
                                        <li><a href="#ng">Price, low to high</a></li>
                                        <li><a href="#ng">Date, old to new</a></li>
                                        <li><a href="#ng">Date, new to old</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row engoc-row-equal">
                        @include('partial.product')
                    </div>
                    <div class="shop-bottom" style="justify-content: center;">
                        <ul class="pagination text-center">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">Next <i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('section.instagram')
@endsection

