@extends('admin/layouts.headersidebar')
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Terms & Conditions</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Terms & Conditions</li>
                        </ol>
                    </nav>
                </div>
                {{-- <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('admin.terms.index') }}" class="btn btn-sm btn-primary" title=""><i
                                class="fa fa-list"></i> Show Terms & Conditions</a>
                </div> --}}
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-12">
                <form method="POST" action="{{ route('admin.terms.update') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                    <div class="body">
                            <div class="row">

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                        <div class="card-header">Content</div>
                                        <div class="body">
                                            <textarea id="ckeditor" name="content">
                                                {!! isset($term->content) ? $term->content:'' !!}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="submit" class="btn btn-success" value="save">Save</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="https://cdn.ckeditor.com/4.14.1/full/ckeditor.js"></script>
    <script>
            CKEDITOR.replace( 'ckeditor' );
    </script>
@endsection