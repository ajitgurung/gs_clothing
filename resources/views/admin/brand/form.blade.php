@extends('admin/layouts.headersidebar')
@section('style')
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}">
@endsection
@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Brands</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.brand.index') }}">Brands</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{ isset($brand) ? 'Update' : 'Create' }}</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('admin.brand.index') }}" class="btn btn-sm btn-primary" title=""><i
                                class="fa fa-list"></i> Show Brands</a>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-12">
                @php
                    $url = route('admin.brand.store');
                if (isset($brand)){
                    $url = route('admin.brand.update', base64_encode($brand->id));
                }
                @endphp
                <form method="POST" action="{{ $url }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="body">
                            <div class="row">

                            <div class="col-md-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                        class="fa fa-text-width fa-lg"></i> &nbsp;Title</span>
                                    </div>
                                    <input type="text" name="title" class="form-control"
                                            aria-label="Default" value="{{ isset($brand) ? $brand->title : '' }}"
                                            aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Image</span>
                                    </div>
                                    <input type="file" class="form-control" name="image">
                                    <label class="badge badge-warning pt-2">Image Size Must be 170 X 100
                                        Px</label>
                                </div>
                                @if(isset($brand))
                                <div style="float: right">
                                    @php($image = isset($brand->image) && $brand->image ? asset('storage/brand/'.$brand->image): '')     
                                    <img src="{{$image}}" alt="" class="image-responsive"
                                         style="width: 50px; height: 50px;">
                                </div>
                                @endif
                            </div>

                                                   
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('admin.brand.index') }}" class="btn btn-danger">Cancel</a>
                                <span class="float-right">
                                <button type="submit" name="submit" class="btn btn-success" value="save">Save</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
        CKEDITOR.replace( 'ckeditor' );
</script>
@endsection
