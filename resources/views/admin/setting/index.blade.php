@extends('admin/layouts.headersidebar')
@section('style')
    <style>
        .img-thumbnail {
            padding: .25rem;
            background-color: #dee2e6;
            border: 1px solid #dee2e6;
            border-radius: .25rem;
            max-width: 100%;
            height: auto;
        }

    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Setting</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Site Setting</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ url('admin') }}" class="btn btn-sm btn-round btn-outline-primary" title=""><i
                            class="fa fa-angle-double-left"></i> Go Back</a>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h6>Update Your Site Setting</h6>
                </div>
                <div class="card-body">
                    <form id="advanced-form" data-parsley-validate="" novalidate=""
                        action="{{ route('admin.setting.update') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">

                                    <div class="card-body ">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-file-image-o"></i>
                                                            &nbsp;Logo </span>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" name="logo" class="custom-file-input"
                                                            id="inputGroupFile03">
                                                        <label class="custom-file-label" for="inputGroupFile03">Choose
                                                            Logo</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                @if (isset($setting->logo))
                                                    <img src="{{ asset('storage/setting/logo/' . $setting->logo) }}"
                                                        data-toggle="tooltip" data-placement="top" title="" alt="Logo"
                                                        class="rounded img-thumbnail" width="80px"
                                                        data-original-title="Logo">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-file-image-o"></i>
                                                            &nbsp;Favicon </span>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" name="favicon" class="custom-file-input"
                                                            id="inputGroupFile03">
                                                        <label class="custom-file-label" for="inputGroupFile03">Choose
                                                            Favicon</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                @if (isset($setting->favicon))
                                                    <img src="{{ asset('storage/setting/favicon/' . $setting->favicon) }}"
                                                        data-toggle="tooltip" data-placement="top" title="" alt="Favicon"
                                                        class="rounded img-thumbnail" width="50px"
                                                        data-original-title="Favicon">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-8">
                                    <div class="card">

                                        <div class="card-body ">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fa fa-file-image-o"></i> &nbsp;Header </span>
                                                        </div>
                                                        <div class="custom-file">
                                                            <input type="file" name="header_image" class="custom-file-input"
                                                                id="inputGroupFile03">
                                                            <label class="custom-file-label" for="inputGroupFile03">Choose Image</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    @if (isset($setting->header_image))
                                                        <img src="{{ asset('storage/setting/header_image/' . $setting->header_image) }}"
                                                            data-toggle="tooltip" data-placement="top" title=""
                                                            alt="header_image" class="rounded img-thumbnail" width="80px"
                                                            data-original-title="header_image">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-info fa-lg"></i> &nbsp;Site Title</span>
                                    </div>
                                    <input type="text" name="sitetitle"
                                        value="{{ isset($setting->sitetitle) ? $setting->sitetitle : '' }}"
                                        class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-envelope fa-lg"></i> &nbsp;Site Email</span>
                                    </div>
                                    <input type="email" name="siteemail"
                                        value="{{ isset($setting->siteemail) ? $setting->siteemail : '' }}"
                                        class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-phone-square fa-lg"></i> &nbsp; Phone</span>
                                    </div>
                                    <input type="number" name="phone"
                                        value="{{ isset($setting->phone) ? $setting->phone : '' }}" class="form-control"
                                        aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-mobile-phone fa-lg"></i> &nbsp; Mobile</span>
                                    </div>
                                    <input type="number" name="mobile"
                                        value="{{ isset($setting->mobile) ? $setting->mobile : '' }}" class="form-control"
                                        aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-phone-square fa-lg"></i> &nbsp; Phone 2</span>
                                    </div>
                                    <input type="number" name="phone2"
                                        value="{{ isset($setting->phone2) ? $setting->phone2 : '' }}" class="form-control"
                                        aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-mobile-phone fa-lg"></i> &nbsp; Mobile 2</span>
                                    </div>
                                    <input type="number" name="mobile2"
                                        value="{{ isset($setting->mobile2) ? $setting->mobile2 : '' }}" class="form-control"
                                        aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-mobile-phone fa-lg"></i> &nbsp; Hotline</span>
                                    </div>
                                    <input type="number" name="hotline"
                                        value="{{ isset($setting->hotline) ? $setting->hotline : '' }}" class="form-control"
                                        aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-map-marker fa-lg"></i> &nbsp; Address</span>
                                    </div>
                                    <input type="text" value="{{ isset($setting->address) ? $setting->address : '' }}"
                                        name="address" class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-facebook-square fa-lg"></i> &nbsp;Facebook Url</span>
                                    </div>
                                    <input type="text" name="facebookurl"
                                        value="{{ isset($setting->facebookurl) ? $setting->facebookurl : '' }}"
                                        class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-instagram fa-lg"></i>&nbsp; Instagram Url</span>
                                    </div>
                                    <input type="text" name="instagramurl"
                                        value="{{ isset($setting->instagramurl) ? $setting->instagramurl : '' }}"
                                        class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-whatsapp fa-lg"></i> &nbsp;Whatsapp no.</span>
                                    </div>
                                    <input type="text" name="whatsapp"
                                        value="{{ isset($setting->whatsapp) ? $setting->whatsapp : '' }}"
                                        class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default">
                                        <img src="{{ asset('backend/assets/images/viber-brands.svg') }}" style="display:inline-block;width:16px;height:16px;opacity:0.6;" />&nbsp; Viber No.</span>
                                    </div>
                                    <input type="text" name="viber"
                                        value="{{ isset($setting->viber) ? $setting->viber : '' }}"
                                        class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-file-text-o fa-lg"></i>&nbsp; About Your Site</span>
                                    </div>
                                    <textarea name="sitekeyword" class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">{{ isset($setting->sitekeyword) ? $setting->sitekeyword : '' }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                class="fa fa-map fa-lg"></i>&nbsp; Google Map Url</span>
                                    </div>
                                    <textarea name="googlemapurl" class="form-control" aria-label="Default"
                                        aria-describedby="inputGroup-sizing-default">{{ isset($setting->googlemapurl) ? $setting->googlemapurl : '' }}</textarea>
                                </div>
                            </div>
                            
                       

                            <div class="col-12">
                                <!-- <button type="submit" class="btn btn-outline-danger">Cancel</button> -->
                                <button style="float: right" type="submit" class="btn btn-outline-success">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
