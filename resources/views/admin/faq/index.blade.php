@extends('admin/layouts.headersidebar')
@section('style')
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/nestable/jquery-nestable.css') }}"/>
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}">
@endsection
@section('content')
    <script>
        $(document).ready(function () {
            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target), output = list.data('output');

                $.ajax({
                    method: "POST",
                    url: "{{route('admin.faq.order')}}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        list_order: list.nestable('serialize'),
                    },
                    success: function (response) {
                        var obj = jQuery.parseJSON(response);
                        if (obj.status == 'success') {
                            swal({
                                title: 'Success!',
                                buttonsStyling: false,
                                confirmButtonClass: "btn btn-success",
                                html: '<b>FAQs</b> Sorted Successfully',
                                timer: 1000,
                                type: "success"
                            }).catch(swal.noop);
                        }
                        ;

                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    sweetAlert('Failure', 'Something Went Wrong!', 'error');
                });
            };

            $('#nestable').nestable({
                group: 1,
                maxDepth: 1,
            }).on('change', updateOutput);
        });
    </script>

    <?php
    function displayList($brands)
    {
    ?>
    <ol class="dd-list">
        <?php foreach ($brands as $item): ?>
        <li class="dd-item dd3-item" data-id="{{ $item->id }} ">
            <div class="dd-handle dd3-handle"></div>
            <div class="dd3-content">
                <small>
                    <b>{{ $item->question }}</b>
                </small>
                <span class="content-right">
                    <a href="{{ route('admin.faq.edit', base64_encode($item->id)) }}"
                        class="btn btn-sm btn-outline-primary" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="#delete"
                        data-toggle="modal"
                        data-id="{{ base64_encode($item->id) }}"
                        id="delete{{ base64_encode($item->id) }}"
                        class="btn btn-sm btn-outline-danger center-block"
                        onClick="delete_menu('{{ base64_encode($item->id) }}')"><i class="fa fa-trash  "></i></a>
                </span>
            </div>
        </li>
        <?php
        endforeach; ?>
    </ol>
    <?php
    }
    ?>

    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>FAQs</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page"> Manage your FAQs</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ url('admin') }}" class="btn btn-outline-primary btn-round"><i
                                class="fa fa-angle-double-left"></i> Go Back</a>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">

                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link show  active" data-toggle="tab" href="#Pages">All
                            FAQs</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#addPage">Add FAQ</a></li>
                </ul>
                <div class="tab-content mt-0">
                    <div class="tab-pane show active" id="Pages">
                        <div class="card">
                            <div class="header card-header">
                                <h6 class="title mb-0">All FAQs</h6>
                            </div>
                            <div class="body mt-0">
                                <div class="dd nestable-with-handle" id="nestable">
                                    <?php isset($items) ? displayList($items) : '' ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane" id="addPage">
                        <div class="card">
                            <div class="header card-header">
                                <h6 class="title mb-0">Add FAQ</h6>
                            </div>
                            <div class="body mt-2">
                                <form method="post" action="{{ route('admin.faq.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                    
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                    class="fa fa-text-width fa-lg"></i> &nbsp;Question</span>
                                                </div>
                                                <input type="text" name="question" class="form-control"
                                                       aria-label="Default"
                                                       aria-describedby="inputGroup-sizing-default">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <input type="checkbox" name="status" checked></span>
                                                </div>
                                                <input type="text" class="form-control" value="Display Status" disabled>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                    class="fa fa-text-width fa-lg"></i> &nbsp;Answer</span>
                                                </div>
                                                <textarea name="answer"class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-12">
                                            <a href="{{ route('admin.brand.index') }}"
                                               class="btn btn-outline-danger">Cancel</a>

                                            <button type="submit" style="float: right;" class="btn btn-outline-success">
                                                save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="clearfix"></div>
            <div class="col-md-12">

            </div>

        </div>

        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content bg-danger">
                    <div class="modal-header">
                        <h5 class="modal-title text-white" id="exampleModalLabel">Delete Brand</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-white">
                        <p>Are you Sure...!!</p>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                        <a href="" class="btn btn-round btn-primary">Delete</a>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
@section('script')
    <script>

        function delete_menu(id) {
            var conn = './brand/delete/' + id;
            $('#delete a').attr("href", conn);
        }

    </script>
    <script src="{{ asset('backend/assets/vendor/nestable/jquery.nestable.js') }}"></script><!-- Jquery Nestable -->
    <script src="{{ asset('backend/html/assets/js/pages/ui/sortable-nestable.js') }}"></script>


@endsection
