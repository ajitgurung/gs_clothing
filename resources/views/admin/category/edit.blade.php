@extends('admin/layouts.headersidebar')
@section('style')
<link rel="stylesheet" href="{{ asset('backend/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Categories</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.category.index') }}">Category</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('admin.category.index') }}" class="btn btn-sm btn-primary" title=""><i
                                class="fa fa-list"></i> Show Category</a>
                </div>
            </div>
        </div>
        <div class="card clearfix">
            <div class="card-body">
                <form method="post" action="{{ route('admin.category.update', base64_encode($category->id)) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                    class="fa fa-text-width fa-lg"></i> &nbsp;Title</span>
                                </div>
                                <input type="text" name="title" class="form-control"
                                        aria-label="Default" value="{{ isset($category->title) ? $category->title : '' }}"
                                        aria-describedby="inputGroup-sizing-default" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Image</span>
                                </div>
                                <input type="file" class="form-control" name="image">
                                <label class="badge badge-warning pt-2">Image Size Must be 270 X 170
                                    Px</label>
                            </div>
                            <div style="float: right">
                                <div class="card">
                                    <div class="card-body">
                                        @php($image = isset($category) && $category->image ? asset('storage/category/'.$category->slug.'/'.$category->image): '')
                                        <img src="{{$image}}" alt="" class="image-responsive"
                                                style="width: 50px; height: 50px;">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <input type="checkbox" name="status" value="1" {{ isset($category->status) && $category->status == 1 ? 'checked':''}}
                                                        aria-label="Checkbox for following text input">
                                            </div>
                                        </div>
                                        <input type="button " class="form-control bg-indigo text-muted"
                                                value="Status" disabled>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <input type="checkbox" name="featured" value="1" {{ isset($category->featured) && $category->featured == 1 ? 'checked':''}}
                                                        aria-label="Checkbox for following text input">
                                            </div>
                                        </div>
                                        <input type="button " class="form-control bg-indigo text-muted"
                                                value="Featured" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                        <div class="clearfix"></div>

                        <div class="col-md-12">
                            <a href="{{ route('admin.category.index') }}"
                                class="btn btn-outline-danger">Cancel</a>

                            <button type="submit" style="float: right;" class="btn btn-outline-success">
                                save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection