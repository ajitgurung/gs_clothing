@extends('admin/layouts.headersidebar')
@section('style')
<link rel="stylesheet" href="{{ asset('backend/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Sizes</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.size.index') }}">Sizes</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('admin.size.index') }}" class="btn btn-sm btn-primary" title=""><i
                                class="fa fa-list"></i> Show Sizes</a>
                </div>
            </div>
        </div>
        <div class="card clearfix">
            <div class="card-body">
                <form method="post" action="{{ route('admin.size.update', base64_encode($size->id)) }}" enctype="multipart/form-data">
                    @csrf
                    @method("PATCH")
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i
                                    class="fa fa-text-width fa-lg"></i> &nbsp;Title</span>
                                </div>
                                <input type="text" name="title" class="form-control"
                                        aria-label="Default" value="{{ isset($size->title) ? $size->title : '' }}"
                                        aria-describedby="inputGroup-sizing-default" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i
                                    class="fa fa-text-width fa-lg"></i> &nbsp;Code</span>
                                </div>
                                <input type="text" name="code" class="form-control"
                                        aria-label="Default" value="{{ isset($size->code) ? $size->code : '' }}"
                                        aria-describedby="inputGroup-sizing-default" required>
                            </div>
                        </div>


                        <div class="clearfix"></div>

                        <div class="col-md-12">
                            <a href="{{ route('admin.size.index') }}"
                                class="btn btn-outline-danger">Cancel</a>

                            <button type="submit" style="float: right;" class="btn btn-outline-success">
                                save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection