@extends('admin/layouts.headersidebar')
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Blogs Section</h1>
                    <small>Manage Your Blogs From Here</small>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Blogs</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right">
                    <a href="{{ route('admin.blog.create') }}" class="btn btn-primary"><i
                                class="fa fa-plus-circle"></i> Create New Blog</a>
                </div>
            </div>
        </div>

        <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <ul class="header-dropdown dropdown">
                                
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                                
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom spacing5">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($items as $item)
                                        <tr>
                                            <td class="w60">
                                               <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top"
                                               title=""
                                               data-original-title="">
                                               <span><img
                                                   src="{{ asset('storage/blog/'. $item->slug .'/small_'. $item->banner) }}"
                                                   class="img-thumbnail"></span>
                                               </div> 
                                            </td>
                                            <td>{{ $item->title }}</td>
                                            <form action="" id="statusForm">
                                                @if($item->status === 1)
                                                    <td class=""><span><a href="javascript:void(0);" onclick="status({{ $item->id }})"><iclass="fa fa-toggle-on text-success fd"></iclass=></a></span>
                                                    </td>
                                                @else
                                                    <td class=""><span><a href="javascript:void(0);" onclick="status({{ $item->id }})"><iclass="fa fa-toggle-off text-danger fd"></iclass=></a></span>
                                                    </td>
                                                @endif
                                            </form>
                                    
                                            <td>
                                                <!-- <a class="btn btn-outline-info btn-sm" href=""><i class="fa fa-eye"></i></a> -->
                                                <a class="btn btn-outline-success btn-sm" href="{{ route('admin.blog.edit', $item->slug) }}"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-outline-danger btn-sm" href="#delete" data-toggle="modal" onclick="delete_product('<?= basename($item->slug); ?>')"><i class="fa fa-trash"></i></a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
    </div>


<div class="modal modal-primary fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to delete blog?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-round btn-outline-primary" data-dismiss="modal">Close</a>
                <a class="btn btn-round btn-danger" href="">Delete It!</a>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<script src="{{ asset('backend/assets/jquery-datatable.js') }}"></script>
<script src="{{ asset('backend/assets/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('backend/assets/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/assets/datatablescripts.bundle.js') }}"></script>

    <script>
        function status(id) {
            $.ajax({
                method: 'POST',
                url: "{{ route('admin.blog.status') }}",
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id
                },
                success: function (response) {
                    console.log("response",response);
                    var obj = jQuery.parseJSON(response);
                    if (obj.success) {
                        toastr.success(obj.success);
                        setTimeout(function () {
                            location.reload();  //Refresh page
                        }, 100);
                    } else if (obj.error) {
                        toastr.error(obj.error);
                    }
                },
                error: function (response){
                    console.log("error",response);
                }
            })
        }

        function delete_product(medium) {
            // alert(file);
            var conn = '/admin/blog/delete/' + medium;
            $('#delete a').attr("href", conn);
        }
    </script>
@endsection

