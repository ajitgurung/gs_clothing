@extends('admin/layouts.headersidebar')
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Blogs</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.blog.index') }}">Blogs</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{ isset($item) ? 'Update' : 'Create' }}</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('admin.blog.index') }}" class="btn btn-sm btn-primary" title=""><i
                                class="fa fa-list"></i> Show Blog</a>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-12">
                @php
                    $url = route('admin.blog.store');
                if (isset($item)){
                    $url = route('admin.blog.update', $item->slug);
                }
                @endphp
                <form method="POST" action="{{ $url }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Title</span>
                                        </div>
                                        <input type="text" class="form-control" name="title"
                                               placeholder="Title" value="{{isset($item->title)? $item->title:''}}"
                                               aria-label="Title"
                                               aria-describedby="basic-addon1" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <input type="checkbox" name="status" {{isset($item->status) && $item->status == 1 ? 'checked' : ''}} ></span>
                                        </div>
                                        <input type="text" class="form-control" value="Display Status" disabled>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Banner</span>
                                        </div>
                                        <input type="file" class="form-control" name="banner">
                                        <label class="badge badge-warning pt-2">Image Size Must be 1200 X 800
                                            Px</label>
                                    </div>
                                    <div style="float: right">
                                        @php($image = isset($item->banner) && $item->banner ? asset('storage/blog/'. $item->slug .'/small_'.$item->banner): '')
                                        @if(isset($item))<img src="{{$image}}" alt="" class="image-responsive"
                                             style="width: 50px; height: 50px;">@endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Short Content</span>
                                        </div>
                                        <textarea class="form-control" name="short_content">{{ isset($item) ? $item->short_content : '' }}</textarea>

                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                        <div class="card-header">Content</div>
                                        <div class="body">
                                            <textarea id="ckeditor" name="content">
                                                {!! isset($item->content) ? $item->content:'' !!}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('admin.blog.index') }}" class="btn btn-danger">Cancel</a>
                                <span class="float-right">
                                <button type="submit" name="submit" class="btn btn-success" value="save">Save</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="https://cdn.ckeditor.com/4.14.1/full/ckeditor.js"></script>
    <script>
            CKEDITOR.replace( 'ckeditor' );
    </script>
@endsection