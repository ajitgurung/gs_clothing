@extends('admin/layouts.headersidebar')
@section('style')
    <style>
        .img-thumbnail {
            padding: .25rem;
            background-color: #dee2e6;
            border: 1px solid #dee2e6;
            border-radius: .25rem;
            max-width: 100%;
            height: auto;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Terms & Condition</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Terms & Condition</li>
                        </ol> 
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('admin.dashboard') }}" class="btn btn-sm btn-round btn-outline-primary" title=""><i
                            class="fa fa-angle-double-left"></i> Go Back</a>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h6>Update Your Terms & Condition</h6>
                </div>
                <div class="card-body">
                    <form id="advanced-form" data-parsley-validate="" novalidate=""
                          action="{{ route('admin.terms.update') }}" method="post">
                        @csrf
                        <div class="card">
                            <div class="body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="card">
                                            <div class="card-header">Content</div>
                                            <div class="body">
                                                <textarea id="ckeditor" name="content">
                                                    {!! isset($term->content) ? $term->content:'' !!}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button style="float: right" type="submit" class="btn btn-outline-success">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- <button type="submit" class="btn btn-outline-danger">Cancel</button> -->
                        
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('script')

    <script src="https://cdn.ckeditor.com/4.14.1/full/ckeditor.js"></script>
    <script>
            CKEDITOR.replace( 'ckeditor' );
    </script>
@endsection

