@extends('admin/layouts.headersidebar')
@section('style')
<link rel="stylesheet" href="{{ asset('backend/assets/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/assets/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/assets/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Products Section</h1>
                    <small>Manage Your Products From Here</small>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Products</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right">
                    <a href="{{ route('admin.product.create') }}" class="btn btn-primary"><i
                                class="fa fa-plus-circle"></i> Create New Product</a>
                </div>
            </div>
        </div>

        <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <ul class="header-dropdown dropdown">
                                
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                                
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom spacing5">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Code</th>
                                            <th>Category</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Featured</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Image</th>
                                            <th>Code</th>
                                            <th>Category</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Featured</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($products as $chunked)
                                        @foreach($chunked as $product)
                                        <tr>
                                            <td class="w60">
                                               <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top"
                                               title=""
                                               data-original-title="{{ $product->name }}">
                                               <span><img
                                                   src="{{ asset('storage/product/'.$product->slug.'/small_'.$product->image) }}"
                                                   class="img-thumbnail"></span>
                                               </div>
                                            </td>
                                            <td>{{ $product->variation == null ? \Illuminate\Support\Str::limit($product->sku, 15, $end='...') : '' }}</td>

                                            <td>{{ $product->category->title }}</td>
                                            
                                            <td>
                                                <div class="font-16">{{ \Illuminate\Support\Str::limit($product->title, 30, $end='...') }}</div>
                                            </td>
                                            <form action="" id="statusForm">
                                                @if($product->display === 1)
                                                    <td class=""><span><a href="#" onclick="status('{{ $product->id }}')"><i
                                                                        class="fa fa-toggle-on text-success fd"></i></a></span>
                                                    </td>
                                                @else
                                                    <td class=""><span><a href="#" onclick="status('{{ $product->id }}')"><i
                                                                        class="fa fa-toggle-off text-danger fd"></i></a></span>
                                                    </td>
                                                @endif
                                            </form>
                                            <form action="" id="statusForm">
                                                @if($product->featured === 1)
                                                    <td class=""><span><a href="#" onclick="featured('{{ $product->id }}')"><i
                                                                        class="fa fa-toggle-on text-success fd"></i></a></span>
                                                    </td>
                                                @else
                                                    <td class=""><span><a href="#" onclick="featured('{{ $product->id }}')"><i
                                                                        class="fa fa-toggle-off text-danger fd"></i></a></span>
                                                    </td>
                                                @endif
                                            </form>
                                              
                                            <td>
                                                <a href="{{ route('admin.product.variation.index', $product->slug) }}" class="btn btn-outline-info btn-sm" href="">Manage Colors and sizes</a>
                                                <a class="btn btn-outline-success btn-sm" href="{{ route('admin.product.edit', $product->id) }}"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-outline-danger btn-sm" href="#delete" data-toggle="modal" onclick="delete_product('<?= basename($product->id); ?>')"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
    </div>


<div class="modal modal-primary fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to delete product?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-round btn-outline-primary" data-dismiss="modal">Close</a>
                <a class="btn btn-round btn-danger" href="">Delete It!</a>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<script src="{{ asset('backend/assets/jquery-datatable.js') }}"></script>
<script src="{{ asset('backend/assets/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('backend/assets/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/assets/datatablescripts.bundle.js') }}"></script>
    <script>
        function status(id) {
            $.ajax({
                method: 'POST',
                url: "{{ route('admin.product.status') }}",
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id
                },
                success: function (response) {
                    var obj = jQuery.parseJSON(response);
                    if (obj.success) {
                        toastr.success(obj.success);
                        setTimeout(function () {
                            location.reload();  //Refresh page
                        }, 100);
                    } else if (obj.error) {
                        toastr.error(obj.error);
                    }
                },
                error: function (response){
                    console.log("error",response);
                }
            })
        }

        function featured(id) {
            $.ajax({
                method: 'POST',
                url: "{{ route('admin.product.featured') }}",
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id
                },
                success: function (response) {
                    var obj = jQuery.parseJSON(response);
                    if (obj.success) {
                        toastr.success(obj.success);
                        setTimeout(function () {
                            location.reload();  //Refresh page
                        }, 100);
                    } else if (obj.error) {
                        toastr.error(obj.error);
                    }
                },
                error: function (response){
                    console.log("error",response);
                }
            })
        }

        function delete_product(medium) {
            // alert(file);
            var conn = '/admin/product/delete/' + medium;
            $('#delete a').attr("href", conn);
        }
    </script>
   
@endsection
