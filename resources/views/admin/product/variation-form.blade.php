@extends('admin.layouts.headersidebar')
@section('style')
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Products</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.product.index') }}">Products</a></li>
                            <li class="breadcrumb-item"><a
                                    href="{{ route('admin.product.variation.index', $slug) }}">Variations</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                {{ isset($variation) ? 'Update' : 'Create' }} </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('admin.product.variation.index', $slug) }}" class="btn btn-sm btn-primary"
                        title=""><i class="fa fa-list"></i> Show Variations</a>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-12">
                @php
                    $url = route('admin.product.variation.store', $slug);
                    if (isset($variation)) {
                        $url = route('admin.product.variation.update', ['slug' => $slug, 'id' => base64_encode($variation->id)]);
                    }
                @endphp
                <form method="POST" action="{{ $url }}" enctype="multipart/form-data">
                    @csrf
                    @if (isset($variation))
                        @method('PATCH')
                    @endif
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Color Title</span>
                                        </div>
                                        <input type="text" class="form-control" name="title" placeholder="Title"
                                            value="{{ isset($product) ? $product->title : '' }}" aria-label="Title"
                                            aria-describedby="basic-addon1" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3 colorpicker">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                    class="fa fa-text-width fa-lg"></i> &nbsp;Code</span>
                                        </div>
                                        <input name="color_code" type="text" class="form-control"
                                            value="{{ $variation->color_code ?? '#00AABB' }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><span
                                                    class="input-group-addon"><i></i></span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Image</span>
                                        </div>
                                        <input type="file" class="form-control" name="image"
                                            {{ isset($product) ? '' : 'required' }}>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="badge badge-warning pt-2">Image Size Must be 600 X 600 Px</label>
                                    @if (isset($product))
                                        <div style="float: right">
                                            <div class="card">
                                                <div class="card-body d-inline-block">
                                                    @php($image = isset($product) && $product->image ? asset('storage/product/' . $product->slug . '/' . substr($variation->color_code, 1) . '/small_' . $variation->image) : '')
                                                    <img src="{{ $image }}" alt="" class="image-responsive"
                                                        style="width: 50px; height: 50px;">
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Gallery Image</span>
                                        </div>
                                        <input type="file" class="form-control" name="gallery_image[]" multiple>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if (isset($product))
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="card">
                                                <div class="row">
                                                    <?php $images = collect(Storage::files('public/product/' . $slug . '/' . substr($variation->color_code, 1) . '/gallery'))->map(function ($file) {
                                                        return Storage::url($file);
                                                    }); ?>
                                                    @foreach ($images as $image)
                                                        <div class="col-md-2">
                                                            <a href="#delete_image" data-toggle="modal" data-photo=""
                                                                onclick="delete_image('<?= basename($image) ?>', '<?= basename($variation->id) ?>')"
                                                                id="" title="Delete Image">
                                                                <i style="position: absolute; top: -9px; padding: 4px; color: #fff;border-radius: 50%; opacity: 1;"
                                                                    class="btn-danger close fa fa-trash"></i>
                                                            </a>
                                                            <img class="img-thumbnail" src="{{ asset($image) }}"
                                                                alt="no-image" style="max-width: 100px; margin-right: 5px;">
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                        <div class="card-header">Sizes</div>
                                        <div class="body">
                                            <div class="row">
                                                @foreach ($sizes as $size)
                                                    <div class="col-md-3">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <input type="checkbox" name="sizes[]"
                                                                        value="{{ $size->id }}"
                                                                        {{ isset($variation) && in_array($size->id, json_decode($variation->sizes)) ? 'checked' : '' }}></span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                value="{{ $size->title }}" disabled>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <a href="{{ route('admin.product.variation.index', $slug) }}"
                                    class="btn btn-danger">Cancel</a>
                                <span class="float-right">
                                    <button type="submit" name="submit" class="btn btn-success"
                                        value="save">Save</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="delete_image">
        <div class="modal-dialog " role="document">
            <div class="modal-content bg-warning">
                <div class="modal-header">
                    <h5 class="modal-title text-white" id="exampleModalLabel">Delete Gallery Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-white">
                    <p>Are you Sure...!!</p>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                    <a href="" class="btn btn-round btn-danger">Delete</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('backend/assets/vendor/dropify/js/dropify.js') }}"></script>
    <script src="{{ asset('backend/assets/dropify.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.category-select').select2();
        });
    </script>
    <script src="{{ asset('backend/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
    <script src="{{ asset('backend/assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script><!-- Bootstrap Colorpicker Js -->

    <script type="text/javascript">
        $(".colorpicker").colorpicker();
    </script>
    <script>
        CKEDITOR.replace('content');


        function delete_image(image, slug) {
            var conn = '/admin/product/delete-variation-image/' + slug + '/' + image;
            $('#delete_image a').attr("href", conn);
        }
    </script>
    <script>
        $(document).on('click', '.btn_remove_color', function() {
            var button_id = $(this).attr("id");
            $('#color-' + button_id).remove();
        });
        var i = 0;
        $('#add-color').click(function() {
            i++;
            $.ajax({
                url: "{{ url('admin/product/add-color/') }}/" + i,
                cache: false,
                beforeSend: function() {

                },
                complete: function($response, $status) {
                    if ($status != "error" && $status != "timeout") {
                        $('#color').append($response.responseText);
                        $(".colorpicker").colorpicker()
                    }
                },
                error: function($responseObj) {
                    alert("Something went wrong while processing your request.\n\nError => " +
                        $responseObj.responseText);
                }
            });
        });
    </script>
@endsection
