@extends('admin.layouts.headersidebar')
@section('style')
<link rel="stylesheet" href="{{ asset('backend/assets/vendor/dropify/css/dropify.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('backend/assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}">
<style>
    .card-header span img {
            width: 15px;
            float: right;
        }

        
        .card-header img{
             transform: rotate(-90deg);
        }
         .card-header .transform-active {
            transform-origin: center;
            transition: 0.1s ease-in-out;
           transform: rotate(0);
        }
        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 35px;
            user-select: none;
            -webkit-user-select: none;
        }
        .select2-container--default .select2-results__option--disabled {
            color: #1a1a1a;
            font-weight: 600;
        }
</style>

@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Products</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.product.index') }}">Product</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">@if(isset($product)) Update @else Create @endif</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ route('admin.product.index') }}" class="btn btn-sm btn-primary" title=""><i class="fa fa-list"></i> Show Products</a>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-12">
                @php
                    $url = route('admin.product.store');
                if (isset($product)){
                    $url = route('admin.product.update',['id'=>base64_encode($product->id)]);
                }
                @endphp
                <form method="POST" action="{{ $url }}" enctype="multipart/form-data">
                    @csrf
                    @if(isset($product))
                    @method('PATCH')
                    @endif
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Title</span>
                                        </div>
                                        <input type="text" class="form-control" name="title"
                                               placeholder="Title" value="{{isset($product)? $product->title:''}}"
                                               aria-label="Title"
                                               aria-describedby="basic-addon1" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">SKU</span>
                                        </div>
                                        <input type="text" class="form-control" name="sku"
                                               placeholder="SKU" value="{{isset($product)? $product->sku:''}}"
                                               aria-label="SKU"
                                               aria-describedby="basic-addon1" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <input type="checkbox" name="display" {{isset($product->display) && $product->display == 0 ? '' : 'checked'}} ></span>
                                                </div>
                                                <input type="text" class="form-control" value="Status" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <input type="checkbox" name="featured" {{isset($product->featured) && $product->featured == 1 ? 'checked' : ''}} ></span>
                                                </div>
                                                <input type="text" class="form-control" value="Featured" disabled>
                                            </div>
                                        </div>    
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Category</span>
                                        </div>
                                        <select name="category_id" id="parent_category" required  class="form-control">
                                            <option value="" selected disabled>Select Category</option>
                                            @foreach($categories as $category)
                                                
                                                @if($category->child == 1 && $category->parent_id == 0)
                                                <option value="" disabled>{{ $category->title }}</option>
                                                @else
                                                <option value="{{ $category->id }}" {{isset($product->category_id) && $product->category_id == $category->id ? 'selected' : ''}}>@if(isset($category->parentCategory))&emsp; {{ $category->parentCategory->title }} => @endif {{ $category->title }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 

                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Brand</span>
                                        </div>
                                        <select name="brand_id" required  class="form-control">
                                            <option value="" selected disabled>Select Brand</option>
                                            @foreach($brands as $brand)
                                                <option value="{{ $brand->id }}" {{isset($product->brand_id) && $product->brand_id == $brand->id ? 'selected' : ''}}>{{ $brand->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Size Chart</span>
                                        </div>
                                        <select name="size_chart_id" id="parent_category" required  class="form-control">
                                            <option value="" selected disabled>Select Size Chart</option>
                                            @foreach($charts as $chart)
                                                <option value="{{ $chart->id }}" {{isset($product->size_chart_id) && $product->size_chart_id == $chart->id ? 'selected' : ''}}> {{ $chart->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Selling Price</span>
                                        </div>
                                        <input type="number" class="form-control" min="1" name="selling_price"
                                               placeholder="Selling Price" value="{{isset($product)? $product->selling_price:''}}"
                                               aria-label="Selling Price"
                                               aria-describedby="basic-addon1" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Marked Price</span>
                                        </div>
                                        <input type="number" class="form-control" min="1" name="marked_price"
                                               placeholder="Marked Price" value="{{isset($product)? $product->marked_price:''}}"
                                               aria-label="Marked Price"
                                               aria-describedby="basic-addon1" required>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Image</span>
                                        </div>
                                        <input type="file" class="form-control" name="image" {{ isset($product) ? '' : 'required' }}>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="badge badge-warning pt-2">Image Size Must be 600 X 600 Px</label>
                                    @if(isset($product))
                                    <div style="float: right">
                                        <div class="card">
                                            <div class="card-body d-inline-block">
                                                @php($image = isset($product) && $product->image ? asset('storage/product/'.$product->slug.'/small_'.$product->image): '')
                                                <img src="{{$image}}" alt="" class="image-responsive" style="width: 50px; height: 50px;">
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Gallery Image</span>
                                        </div>
                                        <input type="file" class="form-control" name="gallery_image[]" multiple>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if(isset($product))
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="card">
                                            <div class="row">
                                                <?php $images = collect(Storage::files('public/product/'. $product->slug.'/gallery'))->map(function($file) {
                                                            return Storage::url($file);
                                                        });?>
                                                @foreach($images as $image)
                                                
                                                    <div class="col-md-2">
                                                        <a href="#delete_image" data-toggle="modal"
                                                            data-photo=""
                                                            onclick="delete_image('<?= basename($image); ?>', '<?= basename($product->slug); ?>')"
                                                            id="" title="Delete Image">
                                                            <i style="position: absolute; top: -9px; padding: 4px; color: #fff;border-radius: 50%; opacity: 1;" class="btn-danger close fa fa-trash"></i>
                                                        </a>
                                                        <img class="img-thumbnail" src="{{ $image }}" alt="no-image" style="max-width: 100px; margin-right: 5px;">
                                                    </div>

                                                
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="col-md-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Short Description</span>
                                        </div>
                                        <input type="text" class="form-control" name="short_desc"
                                               placeholder="Short Description" value="{{isset($product)? $product->short_desc:''}}"
                                               aria-label="Short Description"
                                               aria-describedby="basic-addon1" required>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                        <div class="card-header">Description</div>
                                        <div class="body">
                                            <textarea id="content" name="desc">
                                                {!! isset($product->desc) ? $product->desc : '' !!}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <a href="{{ route('admin.product.index') }}" class="btn btn-danger">Cancel</a>
                                <span class="float-right">
                        <button type="submit" name="submit" class="btn btn-success" value="save">Save</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="delete_image">
        <div class="modal-dialog " role="document">
            <div class="modal-content bg-warning">
                <div class="modal-header">
                    <h5 class="modal-title text-white" id="exampleModalLabel">Delete Gallery Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-white">
                    <p>Are you Sure...!!</p>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                    <a href="" class="btn btn-round btn-danger">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="delete_image">
        <div class="modal-dialog " role="document">
            <div class="modal-content bg-warning">
                <div class="modal-header">
                    <h5 class="modal-title text-white" id="exampleModalLabel">Delete Gallery Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-white">
                    <p>Are you Sure...!!</p>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                    <a href="" class="btn btn-round btn-danger">Delete</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')



<script src="{{ asset('backend/assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('backend/assets/dropify.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
    $('.category-select').select2();
});
</script>
<script src="{{ asset('backend/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js --> 
<script src="{{ asset('backend/assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script><!-- Bootstrap Colorpicker Js --> 

<script type="text/javascript">
    $(".colorpicker").colorpicker();
</script>
<script>
CKEDITOR.replace( 'content' );


function delete_image(image, slug) {
    var conn = '../delete-image/'+slug+'/' + image;
    $('#delete_image a').attr("href", conn);
}

        
</script> 
<script>
$(document).on('click', '.btn_remove_color', function(){
    var button_id = $(this).attr("id");
    $('#color-'+button_id).remove();
});
var i=0;
$('#add-color').click(function(){
    i++;
    $.ajax({
        url : "{{url('admin/product/add-color/')}}/"+i,
        cache : false,
        beforeSend : function (){

        },
        complete : function($response, $status){
            if ($status != "error" && $status != "timeout") {
                $('#color').append($response.responseText);    
                $(".colorpicker").colorpicker()      
            }
        },
        error : function ($responseObj){
            alert("Something went wrong while processing your request.\n\nError => "
                + $responseObj.responseText);
        }
    });
});

</script>
@endsection
