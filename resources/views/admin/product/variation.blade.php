@extends('admin/layouts.headersidebar')
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Products Color & Size Section</h1>
                    <small>Manage Your Products From Here</small>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item" aria-current="page"><a
                                    href="{{ route('admin.product.index') }}">Products</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Colors & Sizes</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right">
                    <a href="{{ route('admin.product.variation.create', $slug) }}" class="btn btn-primary"><i
                            class="fa fa-plus-circle"></i> Add Colors and Sizes</a>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <ul class="header-dropdown dropdown">

                            <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>

                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Color</th>
                                        <th>Sizes</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Color</th>
                                        <th>Sizes</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($variations as $variation)
                                        {{-- @dd($variation) --}}
                                        <tr>
                                            <td class="w60">
                                                <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top"
                                                    title="" data-original-title="">
                                                    <span><img
                                                            src="{{ asset('storage/product/' . $variation->product->slug . '/' . substr($variation->color_code, 1) . '/small_' . $variation->image) }}"
                                                            class="img-thumbnail"></span>
                                                </div>
                                            </td>
                                            <td>{{ $variation->title }}</td>
                                            <td class="w60">
                                                <div class="avtar-pic w35" data-toggle="tooltip" data-placement="top"
                                                    title="" style="background-color:{{ $variation->color_code }}">
                                                </div>
                                            </td>
                                            <?php $sizes =
                                                $variation->sizes !== null
                                                    ? App\Models\Size::whereIn('id', json_decode($variation->sizes))
                                                        ->select('code')
                                                        ->get()
                                                    : ''; ?>
                                            <td>
                                                @foreach ($sizes as $key => $size)
                                                    {{ $size->code }} @if (count($sizes) > 1 && $key < count($sizes))
                                                        ,
                                                    @endif
                                                @endforeach
                                            </td>
                                            <td>
                                                <a class="btn btn-outline-success btn-sm"
                                                    href="{{ route('admin.product.variation.edit', ['slug' => $slug, 'id' => base64_encode($variation->id)]) }}"><i
                                                        class="fa fa-edit"></i></a>
                                                <a class="btn btn-outline-danger btn-sm" href="#delete" data-toggle="modal"
                                                    onclick="delete_product('{{ $slug }}', '<?= base64_encode($variation->id) ?>')"><i
                                                        class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <div class="modal modal-primary fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Variation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete variation?</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-round btn-outline-primary" data-dismiss="modal">Close</a>
                    <a class="btn btn-round btn-danger" href="">Delete It!</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function delete_product(slug, medium) {
            // alert(file);
            var conn = '/admin/product/color-and-sizes/' + slug + '/delete/' + medium;
            $('#delete a').attr("href", conn);
        }
    </script>
@endsection
