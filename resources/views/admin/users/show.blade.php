@extends('admin/layouts.headersidebar')
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Users</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-primary btn-round" title=""><i
                                class="fa fa-angle-double-left"></i> Go Back</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <ul class="list-group ">
                            <li class="list-group-item d-flex justify-content-between align-items-center">Name <span
                                        class="text-right">{{ $user->name }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">Email <span
                                        class="text-right">{{ $user->email }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">Role <span
                                        class="text-right">{{ ucfirst($user->roles->pluck('name')->first()) }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">Created
                                At<span
                                        class="text-right">{{ $user->created_at }}</span></li>     
                            @if($user->hasRole('client'))
                            <li class="list-group-item d-flex justify-content-between align-items-center">Phone<span
                                        class="text-right">{{ $user->userDetails->phone }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">State<span
                                        class="text-right">{{ $user->userDetails->state }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">District<span
                                        class="text-right">{{ $user->userDetails->district }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">Address<span
                                        class="text-right">{{ $user->userDetails->address }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">Secondary Phone<span
                                        class="text-right">{{ $user->userDetails->sec_phone }}</span></li>
                            @endif                   
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection