@extends('admin/layouts.headersidebar')
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Users</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    @if(!request()->routeIs('admin.users.customer'))
                    <a href="{{ request()->routeIs('admin.users.delivery-managers') ? route('admin.users.delivery-manager-create') : route('admin.users.create') }}" class="btn btn-sm btn-primary btn-round" title=""><i
                                class="fa fa-user-plus"></i> Add New</a>
                    @endif
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card planned_task">
                    <div class="header">
                        <h2>Manage Users</h2>
                        <ul class="header-dropdown dropdown">
                            <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                        </ul>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th width="280px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach ($data as $key => $user)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    
                                    <td>
                                        <a class="btn btn-info"
                                           href="{{ route('admin.users.show',$user->id) }}">Show</a>
                                        
                                        <a class="btn btn-primary"
                                           href="{{ request()->routeIs('admin.users.delivery-managers') ? route('admin.users.delivery-manager-edit', $user->id) : route('admin.users.edit',$user->id) }}">Edit</a>
                                        
                                        <a href="#delete" data-toggle="modal" data-id="{{ $user->id }}" id="delete{{ $user->id }}"
                                            class="btn btn-danger" onClick="delete_user({{ $user->id }} )">
                                            Delete
                                        </a>  
                                        
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $data->render() !!}
                    </div>
                </div>
            </div>

        </div>
        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content bg-danger">
                    <div class="modal-header">
                        <h5 class="modal-title text-white" id="exampleModalLabel">Delete User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-white">
                        <p>Are you Sure...!!</p>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                        <a href="" class="btn btn-round btn-primary">Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>

function delete_user(id) {
    var conn = '/admin/users/delete/' + id;
    $('#delete a').attr("href", conn);
}
</script>
@endsection