<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <title>{{ isset($setting->sitetitle) ? $setting->sitetitle:'' }} || Admin Panel</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @if(isset($setting->favicon))
    <link rel="icon" href="{{ asset('storage/setting/favicon/'.$setting->favicon) }}" type="image/x-icon">
    @endif

    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/animate-css/vivify.min.css') }}">
    <!-- MAIN CSS -->

    <link rel="stylesheet" href="{{ asset('backend/html/assets/css/site.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/sweetalert/sweetalert.css') }}"/>
    <link rel="stylesheet" href="{{ asset('backend/assets/css/toastr.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('backend/assets/css/custom.css') }}"/>
    <link rel="stylesheet" href="{{asset('backend/assets/custom.css')}}">
    
     <style>
     .slimScrollBar{
         width: 6px !important;
         opacity:0.75 !important;
     }
    .main-search-group{
        position:relative;
    }
    .search-output{
        position:absolute;
        top:96%;
        left:0;
        background-color:#fff;
        box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
        width:100%;
        max-height:550px;
        overflow-y:scroll;
        display:none;
    }
        .search-results{
            padding:8px 14px;
            
        }
        .search-wrapper{
            display:flex;
            flex-direction:row;
            align-items:center;
            justify-content:center;
            padding:6px 0;
            border-bottom:1px solid #e0dfdf;
        }
        .search-wrapper:last-of-type{
            border-bottom:none;
        }
        .search-code{
            width:15%;
            padding-left:12px;
        }
        .search-category{
            width:15%;
        }
        .search-name{
            width:60%;
        }
        .search-actions{
            width:10%;
        }
        .search-actions a{
            margin-left:12px;
        }
    </style>

    @yield('style')
    <script
            src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
            crossorigin="anonymous"></script>
</head>
<body class="theme-cyan font-montserrat light_version">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>


<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<div id="wrapper">
    <nav class="navbar top-navbar">
        <div class="container-fluid">
            <div class="navbar-left">
                <div class="navbar-btn">
                    <a href="{{ url('admin') }}">{{ isset($setting->sitetitle) ? $setting->sitetitle:'' }}</a>
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>
                    <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <!-- <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="icon-envelope"></i> -->
                            <!-- <span class="notification-dot bg-green">4</span> -->
                        </a>
                
                    </li>
                </ul>
            </div>
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav text-muted">
                        <li><a title="Visit Site" data-toggle="tooltip" data-placement="top" class="icon-menu"
                               href="{{ route('welcome') }}" target="_blank"><i class="icon-screen-desktop text-blue"></i> </a>
                        </li>&nbsp;|&nbsp;
                        <li><a title="Log Out" data-toggle="tooltip" data-placement="top" class="icon-menu"
                               id="nav-logout"><i
                                        class="icon-power text-red"></i></a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </ul>
                </div>
            </div>
        </div>
        <div class="progress-container">
            <div class="progress-bar" id="myBar">
            </div>
        </div>
    </nav>
    <div id="left-sidebar" class="sidebar">
        <div class="navbar-brand">
        @if(isset($setting->favicon))
            <a href="{{ url('admin') }}"><img src="{{ asset('storage/setting/favicon/'.$setting->favicon) }}"
                                              class="img-fluid logo"><span>{{ isset($setting->sitetitle) ? $setting->sitetitle:'' }}</span></a>
        @endif
            <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i
                        class="lnr lnr-menu icon-close"></i></button>
        </div>
        <div class="sidebar-scroll">
            <div class="user-account">
                <div class="user_div">
                    <img src="{{ asset('backend/assets/images/user.png') }}" class="user-photo img-circle"
                         alt="User Profile Picture">
                </div>
                <div class="dropdown">
                    <span>Welcome,</span>
                    <a href="javascript:void(0);"
                       class=" user-name"><strong>{{ \Illuminate\Support\Facades\Auth::user()->name }}</strong></a>
                </div>
            </div>
            <nav id="left-sidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="header"><b>Main</b></li>
                    <li class="{{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="fa fa-tachometer"></i><span>Dashboard</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.setting') ? 'active' : '' }}">
                        <a href="{{ route('admin.setting') }}">
                            <i class="fa fa-cogs"></i><span>Site Info Management</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.users.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-cogs"></i><span>Admin</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.users.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.users.customer') }}">
                            <i class="fa fa-cogs"></i><span>Customers</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.slider.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.slider.index') }}">
                            <i class="fa fa-cogs"></i><span>Sliders</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.category.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.category.index') }}">
                            <i class="fa fa-cogs"></i><span>Categories</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.brand.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.brand.index') }}">
                            <i class="fa fa-cogs"></i><span>Brands</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.sizechart.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.sizechart.index') }}">
                            <i class="fa fa-cogs"></i><span>Size Chart</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.size.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.size.index') }}">
                            <i class="fa fa-cogs"></i><span>Size</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.product.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.product.index') }}">
                            <i class="fa fa-cogs"></i><span>Products</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.blog.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.blog.index') }}">
                            <i class="fa fa-cogs"></i><span>Blogs</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.faq.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.faq.index') }}">
                            <i class="fa fa-cogs"></i><span>FAQs</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.terms') ? 'active' : '' }}">
                        <a href="{{ route('admin.terms') }}">
                            <i class="fa fa-cogs"></i><span>Terms & Conditions</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('admin.policy') ? 'active' : '' }}">
                        <a href="{{ route('admin.policy') }}">
                            <i class="fa fa-cogs"></i><span>Policies</span>
                        </a>
                    </li>
                    
                </ul>
            </nav>
        </div>
    </div>

    <div id="main-content">
      @yield('content')
    </div>

</div>
<!-- Javascript -->
<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<script src="{{ asset('backend/html/assets/bundles/libscripts.bundle.js') }}"></script>
<script src="{{ asset('backend/html/assets/bundles/vendorscripts.bundle.js') }}"></script>

<script src="{{ asset('backend/html/assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('backend/assets/toastr.min.js') }}"></script>
<!--<script src="{{ asset('backend/assets/matchHeight.js') }}"></script>-->
<script src="{{ asset('backend/assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
// <script>
//      $(".matchHeight").matchHeight();
// </script>


@if (session('status'))
    <script>
        $(function () {
            toastr.success("{{ session('status') }}");
        });
    </script>
@endif
@if (session('error'))
    <script>
        $(function () {
            toastr.error("{{ session('error') }}");
        });
    </script>
@endif
@if ($errors->any())
    @foreach ($errors->all() as $key=>$error)
        <script>
            $(function () {
                toastr.error("{{ $error }}");
            });
        </script>
    @endforeach
@endif
<script>
    $("#nav-logout").click(function (e) {
        e.preventDefault()
        swal({
                title: "Are You Sure!",
                text: "Would you like to log out from the system?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
            },
            function (isConfirm) {
                if (isConfirm) {
                    document.getElementById('logout-form').submit();
                }
            })
    });
</script>

@yield('script')
</body>

</html>
