@extends('admin/layouts.headersidebar')
@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Slider</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="javascript::void(0);">Slider</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Update</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="" class="btn btn-sm btn-primary" title=""><i
                                class="fa fa-list"></i> Show Sliders</a>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-12">
                <form method="POST" action="{{ route('admin.slider.update', $slider->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Title</span>
                                        </div>
                                        <input type="text" class="form-control" name="title"
                                               placeholder="Link" value="{{isset($slider->title)? $slider->title:''}}"
                                               aria-label="Link"
                                               aria-describedby="basic-addon1">
                                    </div>
                                </div>

                                
                                 <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <input type="checkbox" name="status" {{isset($slider->status) && $slider->status == 1 ? 'checked' : ''}} ></span>
                                        </div>
                                        <input type="text" class="form-control" value="Display Status" disabled>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Link</span>
                                        </div>
                                        <input type="text" class="form-control" name="link"
                                               placeholder="Link" value="{{isset($slider->link)? $slider->link:''}}"
                                               aria-label="Link"
                                               aria-describedby="basic-addon1">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Image</span>
                                        </div>
                                        <input type="file" class="form-control" name="image">
                                        <!-- <label class="badge badge-warning pt-2">Image Size Must be 1350 X 490
                                            Px</label> -->
                                    </div>
                                    <div style="float: right">
                                        @php($image = isset($slider->image) && $slider->image ? asset('storage/slider/thumbs/'.$slider->image): '')
                                        <img src="{{$image}}" alt="" class="image-responsive"
                                             style="width: 50px; height: 50px;">
                                    </div>
                                </div>    
                                <div class="col-md-6">
                                    <div class="alert alert-warning">[ Best Image Size 1420 X 370 PX ]</div>
                                </div>                        
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('admin.slider.index') }}" class="btn btn-danger">Cancel</a>
                                <span class="float-right">
                                <button type="submit" name="submit" class="btn btn-success" value="save">Save</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
        CKEDITOR.replace( 'ckeditor' );
</script>
@endsection
