@extends('admin/layouts.headersidebar')
@section('style')
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/nestable/jquery-nestable.css') }}"/>
@endsection
@section('content')
    <script>
        $(document).ready(function () {
            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target), output = list.data('output');

                $.ajax({
                    method: "POST",
                    url: "{{route('admin.slider.order')}}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        list_order: list.nestable('serialize'),
                        table: "sliders"
                    },
                    success: function (response) {
                        var obj = jQuery.parseJSON(response);
                        if (obj.status == 'success') {
                            swal({
                                title: 'Success!',
                                buttonsStyling: false,
                                confirmButtonClass: "btn btn-success",
                                html: '<b>Category</b> Sorted Successfully',
                                timer: 1000,
                                type: "success"
                            }).catch(swal.noop);
                        }
                        ;

                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    sweetAlert('Failure', 'Something Went Wrong!', 'error');
                });
            };

            $('#nestable').nestable({
                group: 1,
                maxDepth: 3,
            }).on('change', updateOutput);
        });
    </script>

    <?php
    function displayList($sliders)
    {
    ?>
    <ol class="dd-list">
        <?php foreach ($sliders as $item): ?>
        <li class="dd-item dd3-item" data-id="{{ $item->id }} ">
            <div class="dd-handle dd3-handle"></div>
            <div class="dd3-content">
                <small>{{ $item->title }}</small>/
                <small>
                    <b>{{ isset($item->link) ? $item->link : 'No Link' }}</b>
                </small>
                /
                <small>@if($item->status == 0) <span class="badge badge-warning">Not Displayed</span> @else <span class="badge badge-success">Displayed</span>@endif</small>
                <span class="content-right">
                        <a href="#viewModal"
                           class="btn btn-sm btn-outline-success" data-toggle="modal"
                           data-id="{{ $item->id }} "
                           id="view{{ $item->id }}"
                           onclick="view('{{ $item->link }}','{{ $item->image }}')"
                           title="View"><i class="fa fa-eye"></i></a>
                        <a href="{{ route('admin.slider.edit', $item->id) }}"
                           class="btn btn-sm btn-outline-primary" title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#delete"
                           data-toggle="modal"
                           data-id="{{ $item->id }}"
                           id="delete{{ $item->id }}"
                           class="btn btn-sm btn-outline-danger center-block"
                           onClick="delete_menu('{{ $item->id }}')"><i class="fa fa-trash  "></i></a>
                    </span>
            </div>
        </li>
        <?php
        endforeach; ?>
    </ol>
    <?php
    }
    ?>

    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Sliders</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page"> Manage your sliders</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{ url('admin') }}" class="btn btn-outline-primary btn-round"><i
                                class="fa fa-angle-double-left"></i> Go Back</a>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">

                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link show  active" data-toggle="tab" href="#Pages">All
                            Sliders</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#addPage">Add Slider</a></li>
                </ul>
                <div class="tab-content mt-0">
                    <div class="tab-pane show active" id="Pages">
                        <div class="card">
                            <div class="header card-header">
                                <h6 class="title mb-0">All Sliders</h6>
                            </div>
                            <div class="body mt-0">
                                <div class="dd nestable-with-handle" id="nestable">
                                    <?php isset($sliders) ? displayList($sliders) : '' ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane" id="addPage">
                        <div class="card">
                            <div class="header card-header">
                                <h6 class="title mb-0">Add Slider</h6>
                            </div>
                            <div class="body mt-2">
                                <form method="post" action="{{ route('admin.slider.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                    class="fa fa-text-width fa-lg"></i> &nbsp;Title</span>
                                                </div>
                                                <input type="text" name="title" class="form-control"
                                                       aria-label="Default"
                                                       aria-describedby="inputGroup-sizing-default">
                                            </div>
                                        </div>
                                    
                                        
                                         <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <input type="checkbox" name="status" checked></span>
                                                </div>
                                                <input type="text" class="form-control" value="Display Status" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                    class="fa fa-text-width fa-lg"></i> &nbsp;Link</span>
                                                </div>
                                                <input type="text" name="link" class="form-control"
                                                       aria-label="Default"
                                                       aria-describedby="inputGroup-sizing-default">
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default"><i
                                                    class="fa fa-image fa-lg"></i> &nbsp;Slider</span>
                                                </div>
                                                <input type="file" name="image"
                                                       class="bg-primary text-white form-control">

                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="alert alert-warning">[ Best Image Size 1420 X 370 PX ]</div>
                                        </div>
                                        

            

                                        <div class="clearfix"></div>

                                        <div class="col-md-12">
                                            <a href="{{ route('admin.slider.index') }}"
                                               class="btn btn-outline-danger">Cancel</a>

                                            <button type="submit" style="float: right;" class="btn btn-outline-success">
                                                save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="clearfix"></div>
            <div class="col-md-12">

            </div>

        </div>

        <div class="modal fade " id="launch-pricing-modal" tabindex="-1" role="dialog"
             aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6>View Slider
                            <span id="viewDisplay">
                            </span>
                            
                        </h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body pricing_page text-center pt-4 mb-4">
                        <div class="card ">
                            <div class="card-header">
                                <h5 id="PageTitle"></h5>
                                <p><b>Button Link: </b><span id="btn_link"></span></p>
                            </div>
                        </div>
                        <div class="card-body">
                            <img id="ViewImage" class="img-fluid"
                                    src="" alt="Image not found">
                            <hr style="background-color: deepskyblue;">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="text-align: right;" type="button" data-dismiss="modal"
                                class="btn btn-outline-danger">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content bg-danger">
                    <div class="modal-header">
                        <h5 class="modal-title text-white" id="exampleModalLabel">Delete Slider</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-white">
                        <p>Are you Sure...!!</p>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                        <a href="" class="btn btn-round btn-primary">Delete</a>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
@section('script')
    <script>
        function view(btn_link, image) {
            $('#launch-pricing-modal').modal('show');
            $('#btn_link').html(btn_link);


            $('#ViewImage').attr('src', "{{ asset('storage/slider/')}}/" +"thumbs/" + image);

        }

        function delete_menu(id) {
            var conn = './slider/delete/' + id;
            $('#delete a').attr("href", conn);
        }

    </script>
    <script src="{{ asset('backend/assets/vendor/nestable/jquery.nestable.js') }}"></script><!-- Jquery Nestable -->
    <script src="{{ asset('backend/html/assets/js/pages/ui/sortable-nestable.js') }}"></script>


@endsection
