@foreach ($sizes as $size)
    @if (in_array($size->id, $var_sizes))
        <label for="size1" class="size-label">
            <input class="select-size" type="radio" id="size1" @if ($loop->first) checked @endif
                name="size-select" value="{{ $size->id }}">
            <span class="circle">{{ $size->code }}</span>
        </label>
    @endif
@endforeach
