<div class="image-gallery">
    <div class="main-product-slider">

        <?php $images = collect(Storage::files('public/product/' . $variation->product->slug . '/' . substr($variation->color_code, 1) . '/gallery'))->map(function ($file) {
            return Storage::url($file);
        }); ?>

        <div class="slider-nav">
            @foreach ($images as $image)
                <div class="product0thumbs">
                    <img src="{{ $image }}" alt="" class="img-responsive">
                </div>
            @endforeach
        </div>
        <div class="slider-for">
            @foreach ($images as $image)
            <div class="product-image">
                <img src="{{ $image }}" alt="" class="img-responsive">
            </div>
            @endforeach
        </div>
    </div>
</div>
