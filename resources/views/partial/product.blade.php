@foreach ($products as $product)
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 product-item">
        <div class="product-main-wrap">
            <div class="product-img">
                <a href="{{ route('product', $product->slug) }}"><img
                        src="{{ asset('storage/product/' . $product->slug . '/' . $product->image) }}" alt=""
                        class="img-responsive"></a>
                <div class="product-button-group">
                    <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                        <span class="ri-zoom-in-line"></span>
                    </a>
                    <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                        <span class="ri-heart-line"></span>
                    </a>
                    <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                        <span class="ri-shopping-cart-line"></span>
                    </a>
                </div>
            </div>
            <div class="product-info text-center">
                <h3 class="product-title">
                    <a href="{{ route('product', $product->slug) }}">{{ $product->title }}</a>
                </h3>
                <div class="product-price">
                    <span>Rs. {{ $product->selling_price ?? $product->marked_price }}</span>
                </div>
            </div>
        </div>
    </div>
@endforeach
