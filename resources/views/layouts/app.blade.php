<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>GS Cloth Land</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Gantari:wght@100;200;300;400;500;600;700;800&display=swap"
        rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('storage/setting/favicon/' . $setting->favicon) }}" type="image/png">
    <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    @stack('post-styles')
</head>

<body>
    <!-- push menu-->
    @include('section.push-menu')
    <!-- end push menu-->
    <!-- Push cart -->
    @include('section.push-cart')
    <!-- End pushcart -->
    <!-- Search form -->
    @include('section.search')
    <!-- End search form -->
    <div class="wrappage">
        @include('section.header')
        <!-- /header -->
    </div>
    @yield('content')

    @include('section.footer')
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/jquery-equal-height.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script>
        $(document).scroll(function() {
            var y = $(this).scrollTop();
            if (y > 100) {
                $('#header').addClass("colored");
            } else {
                $('#header').removeClass("colored");
            }
        });
        $(document).ready(function() {
            $('.product-item').jQueryEqualHeight('.product-main-wrap')
            // $('.product-item').jQueryEqualHeight('.product-info')
        })
    </script>
    @stack('post-scripts')
</body>

</html>
