@extends('layouts.app')

@section('content')
    <div class="slide v5">
        <div class="js-slider-v4">
            <div class="slide-img">
                <picture>
                    <source media="(min-width: 1024px)" srcset="{{ asset('img/files/gs-slider1.webp') }}">
                    <img src="{{ asset('img/files/gs-slider1_mobile.webp') }}" class="img-responsive">
                </picture>
                <div class="box-center content4">
                    <a href="" class="tag-title">#Hers</a>
                    <h3>Women's <br> Winter Collection '22</h3>
                    <a href="{{ route('shop') }}" class="zoa-btn zoa-addcart">
                        Shop Now
                    </a>
                </div>
            </div>
            <div class="slide-img">
                <picture>
                    <source media="(min-width: 1024px)" srcset="{{ asset('img/files/gs-slider2.webp') }}">
                    <img src="{{ asset('img/files/gs-slider2_mobile.webp') }}" class="img-responsive">
                </picture>
                <div class="box-center content4">
                    <a href="" class="tag-title">#His</a>
                    <h3>Men's <br> WInter Collection '22</h3>
                    <a href="" class="zoa-btn zoa-addcart">
                        Shop Now
                    </a>
                </div>
            </div>
        </div>
        <div class="custom">
            <div class="pagingInfo"></div>
        </div>
    </div>
    <!-- content -->
    <div class="zoa-collection-2 pad">
        <div class="container container-content">
            <div class="row first">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="collection-img">
                        <a href="" class="hover-images">
                            <img src="{{ asset('img/files/collection1.webp') }}" alt="" class="img-responsive">
                        </a>
                        <div class="content text-center">
                            <h3>trending products</h3>
                            <a href="" class="zoa-btn zoa-addcart no-width">
                                Shop Now
                            </a>
                        </div>
                        <div class="collection-tags tags-label-right v1"><a href="">#Trending</a></div>
                    </div>

                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row second">
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="collection-img">
                                <a href="" class="hover-images">
                                    <img src="{{ asset('img/files/collection4.webp') }}" alt=""
                                        class="img-responsive">
                                </a>
                                <div class="collection-tags tags-label-right v2"><a href="">#HIS</a></div>

                            </div>
                            <div class="content text-center">
                                <h3>For Him</h3>
                                <a href="" class="zoa-btn zoa-addcart no-width">
                                    Shop Now
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="collection-img">
                                <a href="" class="hover-images">
                                    <img src="{{ asset('img/files/collection3.webp') }}" alt=""
                                        class="img-responsive">
                                </a>
                                <div class="collection-tags tags-label-right v2"><a href="">#HERS</a></div>
                            </div>
                            <div class="content text-center">
                                <h3>For Her</h3>
                                <a href="" class="zoa-btn zoa-addcart no-width">
                                    Shop Now
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trend-product pad">
        <div class="container container-content">
            <div class="row first">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="trend-img hover-images">

                        <img class="img-responsive" src="{{ asset('img/files/bybrand.webp') }}" alt="">

                        <div class="box-center align-items-end">
                            <h3 class="zoa-category-box-title">
                                <a href="#">#ShopByBrand</a>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-6 col-xs-12">
                    <div class="row engoc-row-equal">
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 product-item">
                            <div class="product-main-wrap">
                                <div class="product-img has-brand-logo">
                                    <a href="">
                                        <img src="{{ asset('img/files/logo (1).webp') }}" alt=""
                                            class="is-brand-logo">
                                        <img src="{{ asset('img/files/products/16566552881_.jpg') }}" alt=""
                                            class="img-responsive"></a>
                                    <div class="ribbon zoa-sale"><span>-15%</span></div>
                                    <div class="product-button-group">
                                        <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                            <span class="ri-zoom-in-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                            <span class="ri-heart-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                            <span class="ri-shopping-cart-line"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="">Grosgrain tie cotton top</a>
                                    </h3>
                                    <div class="product-price">
                                        <span class="old">Rs. 2275</span>
                                        <span>Rs. 1925</span>
                                    </div>
                                    <div class="color-group">
                                        <a href="#" class="circle gray"></a>
                                        <a href="#" class="circle yellow"></a>
                                        <a href="#" class="circle white"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 product-item">
                            <div class="product-main-wrap">
                                <div class="product-img has-brand-logo">
                                    <a href="">
                                        <img src="{{ asset('img/files/logo (1).webp') }}" alt=""
                                            class="is-brand-logo">
                                        <img src="{{ asset('img/files/products/16566556770_.jpg') }}" alt=""
                                            class="img-responsive"></a>
                                    <div class="ribbon zoa-hot"><span>Hot</span></div>
                                    <div class="product-button-group">
                                        <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                            <span class="ri-zoom-in-line"></i>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                            <span class="ri-heart-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                            <span class="ri-shopping-cart-line"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="">Grosgrain tie cotton top</a>
                                    </h3>
                                    <div class="product-price">
                                        <span>Rs. 1925</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 product-item">
                            <div class="product-main-wrap">
                                <div class="product-img has-brand-logo">
                                    <a href="">
                                        <img src="{{ asset('img/files/logo (1).webp') }}" alt=""
                                            class="is-brand-logo">
                                        <img src="{{ asset('img/files/products/16568279280_.jpg') }}" alt=""
                                            class="img-responsive"></a>
                                    <div class="ribbon zoa-new"><span>New</span></div>
                                    <div class="product-button-group">
                                        <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                            <span class="ri-zoom-in-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                            <span class="ri-heart-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                            <span class="ri-shopping-cart-line"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="">Grosgrain tie cotton top</a>
                                    </h3>
                                    <div class="product-price">
                                        <span>Rs. 1925</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 product-item">
                            <div class="product-main-wrap">
                                <div class="product-img has-brand-logo">
                                    <a href="">
                                        <img src="{{ asset('img/files/logo (1).webp') }}" alt=""
                                            class="is-brand-logo">
                                        <img src="{{ asset('img/files/products/16576166430_.jpg') }}" alt=""
                                            class="img-responsive"></a>
                                    <div class="product-button-group">
                                        <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                            <span class="ri-zoom-in-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                            <span class="ri-heart-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                            <span class="ri-shopping-cart-line"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="">Grosgrain tie cotton top</a>
                                    </h3>
                                    <div class="product-price">
                                        <span>Rs. 1925</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 product-item">
                            <div class="product-main-wrap">
                                <div class="product-img has-brand-logo">
                                    <a href="">
                                        <img src="{{ asset('img/files/logo (1).webp') }}" alt=""
                                            class="is-brand-logo">
                                        <img src="{{ asset('img/files/products/Golf Umbrella (KGU1916-11a)1.jpg') }}"
                                            alt="" class="img-responsive"></a>
                                    <div class="product-button-group">
                                        <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                            <span class="ri-zoom-in-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                            <span class="ri-heart-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                            <span class="ri-shopping-cart-line"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="">Grosgrain tie cotton top</a>
                                    </h3>
                                    <div class="product-price">
                                        <span>Rs. 1925</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 product-item">
                            <div class="product-main-wrap">
                                <div class="product-img has-brand-logo">
                                    <a href="">
                                        <img src="{{ asset('img/files/logo (1).webp') }}" alt=""
                                            class="is-brand-logo">
                                        <img src="{{ asset('img/files/products/p1_175.webp') }}" alt=""
                                            class="img-responsive"></a>
                                    <div class="ribbon zoa-new"><span>trend</span></div>
                                    <div class="product-button-group">
                                        <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                            <span class="ri-zoom-in-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                            <span class="ri-heart-line"></span>
                                        </a>
                                        <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                            <span class="ri-shopping-cart-line"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-info text-center">
                                    <h3 class="product-title">
                                        <a href="">Grosgrain tie cotton top</a>
                                    </h3>
                                    <div class="product-price">
                                        <span>Rs. 1925</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        $products = App\Models\Product::where('display', 1)->get();
    @endphp
    <div class="zoa-product pad6">
        <div class="container">
            <div class="row">
                @foreach ($products as $product)
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 product-item">
                        <div class="product-main-wrap">
                            <div class="product-img">
                                <a href="{{ route('product', $product->slug) }}"><img
                                        src="{{ asset('storage/product/' . $product->slug . '/' . $product->image) }}"
                                        alt="" class="img-responsive"></a>
                                <div class="product-button-group">
                                    <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                        <span class="ri-zoom-in-line"></span>
                                    </a>
                                    <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                        <span class="ri-heart-line"></span>
                                    </a>
                                    <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                        <span class="ri-shopping-cart-line"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="product-info text-center">
                                <h3 class="product-title">
                                    <a href="{{ route('product', $product->slug) }}">{{ $product->title }}</a>
                                </h3>
                                <div class="product-price">
                                    <span>Rs. {{ $product->selling_price ?? $product->marked_price }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @include('section.instagram')
@endsection
