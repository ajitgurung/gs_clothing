@extends('layouts.app')

@section('content')
    <div class="shop-heading text-center">
        <h1>{{$product->category->title}}</h1>
        <ul class="breadcrumb text-center">
            <li><a href="{{ route('welcome') }}">Home</a></li>
            <li><a href="#"><i class="ri-arrow-right-s-line"></i>&nbsp;{{$product->category->title}}</a></li>
            <li class="active"><i class="ri-arrow-right-s-line"></i>&nbsp;{{$product->title}}</li>
        </ul>
    </div>
    <div class="container container-content">
        <div class="single-product-detail">
            <div class="row">
                {{-- <div class="col-xs-12 col-sm-6 col-md-6"> --}}
                <div class="col-xs-12 col-sm-6 col-md-6" id="variation-slider" data-id={{ base64_encode($product->id) }}>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="single-product-info product-info product-grid-v2">
                        <h3 class="product-title"><a href="#">{{ $product->title ?? '' }}</a></h3>
                        <div class="product-tags">
                            <div class="element-tag">
                                <label>SKU :</label>
                                <span>{{ $product->sku }}</span>
                            </div>
                        </div>
                        <div class="product-price">
                            <span class="old thin">Rs. {{ $product->marked_price }}</span>
                            <span>Rs. {{ $product->selling_price }}</span>
                        </div>

                        <div class="short-desc">
                            <p class="product-desc">{{ $product->short_desc }}</p>
                        </div>
                        <div class="color-group">
                            <label>Color :</label>
                            @foreach ($product->variations as $item)
                                <label for="color1" class="color-label">
                                    <input class="select-color" type="radio"
                                        @if ($loop->first) checked @endif name="color-select"
                                        value="{{ base64_encode($item->id) }}">
                                    <span style="background-color: {{ $item->color_code }}" class="circle gray"></span>
                                </label>
                            @endforeach

                        </div>
                        <div class="color-group">
                            <label>Sizes :</label>
                            <div id="size-section">

                            </div>
                        </div>
                        <div class="single-product-button-group">
                            <div class="flex align-items-center element-button">
                                <div class="zoa-qtt">
                                    <button type="button" class="quantity-left-minus btn btn-number js-minus"
                                        data-type="minus" data-field="">
                                        <small><i class="ri-subtract-line"></i></small>
                                    </button>
                                    <input type="text" name="number" value="1"
                                        class="product_quantity_number js-number">
                                    <button type="button" class="quantity-right-plus btn btn-number js-plus"
                                        data-type="plus" data-field="">
                                        <small><i class="ri-add-line"></i></small>
                                    </button>
                                </div>
                                <a href="" class="zoa-btn zoa-addcart">
                                    <i class="ri-shopping-cart-line"></i>add to cart
                                </a>
                            </div>
                            <a href="" class="btn-wishlist">+ Add to wishlist</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-product-tab bd-bottom">
            <h3>Product Description</h3>
            <div class="content-desc text-left">
                {!! $product->desc ?? '' !!}
            </div>
        </div>
        <div class="container container-content">
            <div class="product-related">
                <h3 class="related-title text-center">You May Also Like</h3>
                <div class="owl-carousel owl-theme owl-cate v2 js-owl-cate">
                    <div class="product-item">
                        <div class="product-img">
                            <a href=""><img src="img/files/products/product_thumb5.webp" alt=""
                                    class="img-responsive"></a>
                            <div class="ribbon zoa-sale"><span>-15%</span></div>
                            <div class="product-button-group">
                                <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                    <span class="ri-zoom-in-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                    <span class="ri-heart-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                    <span class="ri-shopping-cart-line"></span>
                                </a>
                            </div>
                        </div>
                        <div class="product-info text-center">
                            <h3 class="product-title">
                                <a href="">Grosgrain tie cotton top</a>
                            </h3>
                            <div class="product-price">
                                <span class="old">$25.5</span>
                                <span>$20.9</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <a href=""><img src="img/files/products/product_thumb4.webp" alt=""
                                    class="img-responsive"></a>
                            <div class="product-button-group">
                                <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                    <span class="ri-zoom-in-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                    <span class="ri-heart-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                    <span class="ri-shopping-cart-line"></span>
                                </a>
                            </div>
                        </div>
                        <div class="product-info text-center">
                            <h3 class="product-title">
                                <a href="">Grosgrain tie cotton top</a>
                            </h3>
                            <div class="product-price">
                                <span>$20.9</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <a href=""><img src="img/files/products/product_thumb3.webp" alt=""
                                    class="img-responsive"></a>
                            <div class="ribbon zoa-sale"><span>-15%</span></div>
                            <div class="product-button-group">
                                <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                    <span class="ri-zoom-in-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                    <span class="ri-heart-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                    <span class="ri-shopping-cart-line"></span>
                                </a>
                            </div>
                        </div>
                        <div class="product-info text-center">
                            <h3 class="product-title">
                                <a href="">Grosgrain tie cotton top</a>
                            </h3>
                            <div class="product-price">
                                <span class="old">$25.5</span>
                                <span>$20.9</span>
                            </div>
                            <div class="color-group">
                                <a href="#" class="circle gray"></a>
                                <a href="#" class="circle yellow active"></a>
                                <a href="#" class="circle white"></a>
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <a href=""><img src="img/files/products/thumb_1657785984.jpg" alt=""
                                    class="img-responsive"></a>
                            <div class="ribbon zoa-hot"><span>hot</span></div>
                            <div class="product-button-group">
                                <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                    <span class="ri-zoom-in-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                    <span class="ri-heart-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                    <span class="ri-shopping-cart-line"></span>
                                </a>
                            </div>
                        </div>
                        <div class="product-info text-center">
                            <h3 class="product-title">
                                <a href="">Grosgrain tie cotton top</a>
                            </h3>
                            <div class="product-price">
                                <span>$20.9</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <a href=""><img src="img/files/products/thumb_1649851151.jpg" alt=""
                                    class="img-responsive"></a>
                            <div class="ribbon zoa-new"><span>new</span></div>
                            <div class="product-button-group">
                                <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                    <span class="ri-zoom-in-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                    <span class="ri-heart-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                    <span class="ri-shopping-cart-line"></span>
                                </a>
                            </div>
                        </div>
                        <div class="product-info text-center">
                            <h3 class="product-title">
                                <a href="">Grosgrain tie cotton top</a>
                            </h3>
                            <div class="product-price">
                                <span>$20.9</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <a href=""><img src="img/files/products/thumb_1652695072.png" alt=""
                                    class="img-responsive"></a>
                            <div class="ribbon zoa-new"><span>new</span></div>
                            <div class="product-button-group">
                                <a href="#" class="zoa-btn zoa-quickview" title="Quick View">
                                    <span class="ri-zoom-in-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-wishlist" title="Add To  Wishlist">
                                    <span class="ri-heart-line"></span>
                                </a>
                                <a href="#" class="zoa-btn zoa-addcart" title="Add To Cart">
                                    <span class="ri-shopping-cart-line"></span>
                                </a>
                            </div>
                        </div>
                        <div class="product-info text-center">
                            <h3 class="product-title">
                                <a href="">Grosgrain tie cotton top</a>
                            </h3>
                            <div class="product-price">
                                <span>$20.9</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('section.instagram')
    <div id="fixed-social">
        <div>
            <a href="#" class="fixed-call"><i class="ri-facebook-fill"></i><span>Share On
                    Facebook</span></a>
        </div>
        <div>
            <a href="m#" class="fixed-email"><i class="ri-twitter-fill"></i>
                <span>Share On Twitter</span></a>
        </div>
        <div>
            <a href="#" class="fixed-whatsapp"><i class="ri-whatsapp-fill"></i>
                <span>Share On Whatsapp</span></a>
        </div>

    </div>
@endsection

@push('post-scripts')
    <script>
        $(document).ready(function() {
            variation_info();
        })

        $('.select-color').on('click', function(e) {
            variation_info();
        });

        function variation_info() {
            var id = $('input[name="color-select"]:checked').val();
            $.ajax({
                method: 'POST',
                url: "{{ route('product.variation') }}",
                data: {
                    '_token': "{{ csrf_token() }}",
                    'id': id
                },
                success: function(response) {
                    $('#variation-slider').html(response.slider);
                    $('#size-section').html(response.size);

                    function productSlickCall() {
                        $('.slider-for').slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                            fade: true,
                            asNavFor: '.slider-nav'
                        });
                        $('.slider-nav').slick({
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            asNavFor: '.slider-for',
                            dots: false,
                            focusOnSelect: true,
                            vertical: true,
                            verticalSwiping: true,
                            infinite: false,
                            arrows: true,
                            prevArrow: "<span class='previous-arrow arrows'><img src='{{ asset('img/files/chevron-down.svg') }}' alt='prev-arrow'></span>",
                            nextArrow: "<span class='next-arrow arrows'><img src='{{ asset('img/files/chevron-down.svg') }}' alt='prev-arrow'></span>"
                        });
                    }
                    $('.slider-for').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        fade: true,
                        asNavFor: '.slider-nav'
                    });
                    $('.slider-nav').slick({
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        asNavFor: '.slider-for',
                        dots: false,
                        focusOnSelect: true,
                        vertical: true,
                        verticalSwiping: true,
                        infinite: false,
                        arrows: true,
                        prevArrow: "<span class='previous-arrow arrows'><img src='{{ asset('img/files/chevron-down.svg') }}' alt='prev-arrow'></span>",
                        nextArrow: "<span class='next-arrow arrows'><img src='{{ asset('img/files/chevron-down.svg') }}' alt='prev-arrow'></span>"
                    });
                },
                error: function(response) {
                    console.log("error", response);
                }
            })
        }
    </script>
@endpush
